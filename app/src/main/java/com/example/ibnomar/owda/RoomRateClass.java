package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by islam on 28/06/17.
 */

public class RoomRateClass extends AsyncTask<String,Void,String> {

    private Context context;
    ProgressDialog loading;
    String theMail;
    public String reus;
    String room ;
    String user ;
    private TextView textViewJSON;

    public RoomRateClass(Context context, TextView textViewJSON) {

        this.context = context;
        this.textViewJSON=textViewJSON;

    }

    @Override
    protected void onPreExecute() {
        loading=  ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }




    @Override
    protected String doInBackground(String... arg0) {
        String room_id = arg0[0];
        String user_id = arg0[1];
        String rate=arg0[2];
        String comment = arg0[3];

        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "user_id=" + URLEncoder.encode(user_id, "UTF-8");
            data += "&room_id=" + URLEncoder.encode(room_id, "UTF-8");
            data += "&rate=" + URLEncoder.encode(rate, "UTF-8");
            data += "&comment=" + URLEncoder.encode(comment, "UTF-8");


            //   link = "http://10.0.2.2:8080/LoginGP.php" + data;
            //  link = "http://10.0.2.2:8012/LoginGP.php" + data;
            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            url = new URL("http://owda.esy.es/owda/backend/room/rate-room");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();
            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;

        } catch (Exception e) {
            //Toast.makeText(context, "This User Name or Email or Phone Is already taken", Toast.LENGTH_SHORT).show();

            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;




        // this.textViewJSON.setText(result);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);

                int error = jsonObj.getInt("error");

                if (error == 0) {
                    String user_id = jsonObj.getString("user_id");
                    String room_id = jsonObj.getString("room_id");
                    Toast.makeText(context, "Rate inserted successfully.", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context, seeker.class);
                    myIntent.putExtra("room_id", room_id);
                    myIntent.putExtra("id", user_id);
                    context.startActivity(myIntent);
                } else if (error == 1) {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(context,Themail , Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(context, "Couldn't connect to remote database.", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }

}