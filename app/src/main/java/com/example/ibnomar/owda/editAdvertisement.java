package com.example.ibnomar.owda;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class editAdvertisement extends AppCompatActivity {

    EditText NumOfBeds;
    EditText AvailableBeds;
    EditText RoomUsage;
    EditText AdTitle;
    EditText AdDiscribtion;
    EditText AdPrice;
    EditText StartDate;
    EditText EndDate;
    TextView test2;
    RadioGroup HasBalcony,HasBath,InAdvancePay,includeMaintenance;
    Context context;
    String id;
    String Adv_id;
    String user_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_advertisement);
        NumOfBeds = (EditText) findViewById(R.id.NumOfBeds);
        AvailableBeds = (EditText) findViewById(R.id.AvailableBeds);
        RoomUsage = (EditText) findViewById(R.id.RoomUsage);
        AdTitle = (EditText) findViewById(R.id.AdTitle);
        test2 = (TextView) findViewById(R.id.test1);
        AdDiscribtion=(EditText)findViewById(R.id.AdDiscribtion);
        AdPrice=(EditText)findViewById(R.id.AdPrice);
        StartDate=(EditText)findViewById(R.id.StartDate);
        EndDate=(EditText)findViewById(R.id.EndDate);
        HasBalcony=(RadioGroup)findViewById(R.id.HasBalcony);
        HasBath=(RadioGroup)findViewById(R.id.HasBath);
        InAdvancePay=(RadioGroup)findViewById(R.id.InAdvancePay);
        includeMaintenance=(RadioGroup)findViewById(R.id.includeMaintenance);
        Bundle extras = getIntent().getExtras();
        //id = extras.getString("id");
        Adv_id = extras.getString("Adv_id");
        user_id = extras.getString("user_id");
        new ViewEditAd(this, NumOfBeds, AvailableBeds, RoomUsage,  HasBath,HasBalcony,StartDate, EndDate,AdTitle, AdDiscribtion, AdPrice,  InAdvancePay, includeMaintenance ).execute(Adv_id);



        Button Submit = (Button) findViewById(R.id.Update);
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int radioButtonID1 = HasBalcony.getCheckedRadioButtonId();
                View radioButton1 = HasBalcony.findViewById(radioButtonID1);
                int Balcon = HasBalcony.indexOfChild(radioButton1);

                //int gender =radioSex.getCheckedRadioButtonId();

                int radioButtonID2 = HasBath.getCheckedRadioButtonId();
                View radioButton2 = HasBath.findViewById(radioButtonID2);
                int Bath = HasBath.indexOfChild(radioButton2);
                //int status =mStatus.getCheckedRadioButtonId();

                int radioButtonID3 = InAdvancePay.getCheckedRadioButtonId();
                View radioButton3 = InAdvancePay.findViewById(radioButtonID3);
                int AdvancePay = InAdvancePay.indexOfChild(radioButton3);
                //int Smoke =smoke.getCheckedRadioButtonId();

                int radioButtonID4 = includeMaintenance.getCheckedRadioButtonId();
                View radioButton4 = includeMaintenance.findViewById(radioButtonID4);
                int Maintenance = includeMaintenance.indexOfChild(radioButton4);
                //int edu =education.getCheckedRadioButtonId();


                String NumBeds = NumOfBeds.getText().toString();
                String AvailBeds = AvailableBeds.getText().toString();
                String Usage = RoomUsage.getText().toString();
                String Title = AdTitle.getText().toString();
                String Discribtion = AdDiscribtion.getText().toString();
                String Price = AdPrice.getText().toString();
                String From = StartDate.getText().toString();
                String To = EndDate.getText().toString();
                String pic= "null";
                Edit(v,NumBeds,AvailBeds,Usage,Bath,Balcon,From,To,pic,Title,Discribtion,Price,AdvancePay,Maintenance,Adv_id);
            }
        });
    }

            public void Edit(View v, String numBeds, String numAvailable ,String roomUse, int Bath, int Balcon,/*String rateString Reserve*/String StartDate, String EndDate, String pic, String AdTititle, String AdBody, String Price, int inAPay, int Maintenence,String Adv_id) {

                Boolean t = isNetworkConnected();
                if (!numBeds.equals("") && !numAvailable.equals("") /*&& !pic.equals("")  !Reserve.equals("") */ && !Price.equals("") && !AdBody.equals("") && !AdTititle.equals("")) {
                    if (t == true) {
                        new EditAd(this, test2).execute(numBeds, numAvailable, roomUse, String.valueOf(Bath), String.valueOf(Balcon)/*, rate Reserve*/, StartDate, EndDate, pic, AdTititle, AdBody, Price, String.valueOf(inAPay), String.valueOf(Maintenence),Adv_id,user_id);
                        if (test2.getText() == "successfully") {

                    /*Intent i = new Intent(getApplicationContext(),Login.class);
                    startActivity(i);
                    Intent secondActivity = new Intent(MainActivity.this,
                            LiveStream.class);
                    startActivity(secondActivity);*/
                        }


                    } else {
                        test2.setText("Network Failed");

                    }
                } else {

            /*Context context = getApplicationContext();
            CharSequence text = "Please Fill all Fields";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();*/
                    if (NumOfBeds.getText().toString().length() == 0) {
                        NumOfBeds.setError("Number of Beds Field is Required");
                        NumOfBeds.requestFocus();
                    } else if (AvailableBeds.getText().toString().length() == 0) {
                        AvailableBeds.setError("Number of Available Beds Field is Required");
                        AvailableBeds.requestFocus();
                    } else if (AdPrice.getText().toString().length() == 0) {
                        AdPrice.setError("Please confirm password");
                        AdPrice.requestFocus();
                    } else if (AdDiscribtion.getText().toString().length() == 0) {
                        AdDiscribtion.setError("Advertisement's Body is required");
                        AdDiscribtion.requestFocus();


                    }  else if (AdTitle.getText().toString().length() == 0) {
                        AdTitle.setError("Advertisement's Title Is Required");
                        AdTitle.requestFocus();
                    }

                }
            }

            private boolean isNetworkConnected() {
                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

                return cm.getActiveNetworkInfo() != null;
            }
        }
