package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by ahmed on 03/07/17.
 */

public class notifyUser  extends AsyncTask<String,Void,String> {

    private Context context;
    ProgressDialog loading;
    public String reus;
    private TextView textViewJSON1;
    private TextView textViewJSON2;
    private TextView textViewJSON3;

    String Themail;
    private ImageView imageView;
    private Bitmap bitmap;
    //  MyAdapter3 myAdapter3;
String user;


    public notifyUser(Context context, TextView textViewJSON1 ,TextView textViewJSON2,TextView textViewJSON3) {
        this.context = context;
        this.textViewJSON1 = textViewJSON1;
        this.textViewJSON2 = textViewJSON2;
        this.textViewJSON3 = textViewJSON3;


    }


    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }


    @Override
    protected String doInBackground(String... arg0) {
        String id = arg0[0];
         user = arg0[1];


        //Themail =id;
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "user_id=" + URLEncoder.encode(id, "UTF-8");

            URL url = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            //String parameters = "email="+email+"&password="+password;

            //url = new URL("http://owda.esy.es/owda/backend/user/get-user-by-id");
            url = new URL("http://owda.esy.es/owda/backend/user/user-stats");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();

            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;


        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
        //this.textViewJSON1.setText(result);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String msg = jsonObj.getString("messages_count");
                String adv = jsonObj.getString("advertisments_count");
                String active_Adv= jsonObj.getString("active_advertisments");
                if(user.equals("2")) {
                    textViewJSON1.setText(msg);
                    textViewJSON2.setText(adv);
                    textViewJSON3.setText(active_Adv);
                }
                else if(user.equals("1")){
                  textViewJSON1.setText(msg);
                    //textViewJSON2.setText(adv);
                    //textViewJSON3.setText(active_Adv);

                }

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }
}