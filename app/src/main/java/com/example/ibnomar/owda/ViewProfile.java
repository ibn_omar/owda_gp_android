package com.example.ibnomar.owda;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by BoDy on 23/07/2016.
 */
public class ViewProfile extends AsyncTask<String,Void,String> {

    private Context context;
    ProgressDialog loading;
    public String reus;
    private TextView textViewJSON1;
    private TextView textViewJSON2;
    private TextView textViewJSON3;
    private TextView textViewJSON4;
    //private TextView textViewJSON5;
    private TextView textViewJSON6;
    private TextView textViewJSON7;
    private TextView textViewJSON8;
    private TextView textViewJSON9;
    private TextView textViewJSON10;
    private TextView textViewJSON11;
    private TextView textViewJSON12;
    private TextView textViewJSON13;
    private TextView textViewJSON14;
    String Themail;
    private ImageView imageView;
    private Bitmap bitmap;
  //  MyAdapter3 myAdapter3;



    public ViewProfile(Context context, TextView textViewJSON1 ,TextView textViewJSON2,TextView textViewJSON3,TextView textViewJSON4,TextView textViewJSON6,TextView textViewJSON7,TextView textViewJSON8,TextView textViewJSON9,TextView textViewJSON10,TextView textViewJSON11,TextView textViewJSON12,TextView textViewJSON13,TextView textViewJSON14 , ImageView imageView) {
        this.context = context;
        this.textViewJSON1 = textViewJSON1;
        this.textViewJSON2 = textViewJSON2;
        this.textViewJSON3 = textViewJSON3;
        this.textViewJSON4 = textViewJSON4;
        //this.textViewJSON5 = textViewJSON5;
        this.textViewJSON6 = textViewJSON6;
        this.textViewJSON7 = textViewJSON7;
        this.textViewJSON8 = textViewJSON8;
        this.textViewJSON9 = textViewJSON9;
        this.textViewJSON10 = textViewJSON10;
        this.textViewJSON11 = textViewJSON11;
        this.textViewJSON12 = textViewJSON12;
        this.textViewJSON13 = textViewJSON13;
        this.textViewJSON14 = textViewJSON14;
        this.imageView = imageView;


   //     myAdapter3=new MyAdapter3(context);

    }


    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }


    @Override
    protected String doInBackground(String... arg0) {
        String id = arg0[0];


        //Themail =id;
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
           data = "id=" + URLEncoder.encode(id, "UTF-8");

            URL url = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            //String parameters = "email="+email+"&password="+password;

            //url = new URL("http://owda.esy.es/owda/backend/user/get-user-by-id");
            url = new URL("http://owda.esy.es/owda/backend/user/get-user");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();

            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;


        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
       //this.textViewJSON1.setText(result);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String id = jsonObj.getString("id");
                String picture = jsonObj.getString("picture");
                /*if ( bitmap != null) {
                    if (imageView != null) {
                        imageView.setImageBitmap(bitmap);
                    }
                }

*/
                //Picasso.with(context).load(picture).into(imageView);
                String fname = jsonObj.getString("fname");
                String lname = jsonObj.getString("lname");
                String username = jsonObj.getString("username");
                String email = jsonObj.getString("email");
                String password = jsonObj.getString("password");
                String fb_account = jsonObj.getString("fb_account");
                String phone = jsonObj.getString("phone");
                String Nationality = jsonObj.getString("Nationality");
                int educational_level = jsonObj.getInt("educational_level");
               // int educational_level= jsonObj.getInt("educational_level");
                String age = jsonObj.getString("age");
                //String gender = jsonObj.getString("gender");
                int gender= jsonObj.getInt("gender");
                String rate = jsonObj.getString("rate");
                //String marital_status = jsonObj.getString("marital_status");
                int marital_status= jsonObj.getInt("marital_status");
                //String is_smoker = jsonObj.getString("is_smoker");
                int is_smoker= jsonObj.getInt("is_smoker");
                textViewJSON1.setText(fname);
                textViewJSON2.setText(lname);
                textViewJSON3.setText(username);
                textViewJSON4.setText(email);
                //textViewJSON5.setText(password);
                if(fb_account.equals("")){
                    textViewJSON6.setText("Not Avaliable");

                }
                else{
                    textViewJSON6.setText(fb_account);
                }
                textViewJSON7.setText(phone);
                textViewJSON8.setText(Nationality);
                if(educational_level==1){
                    textViewJSON9.setText("Student");
                }
                else if(educational_level==2){
                    textViewJSON9.setText("Bachelor");
                }
                else if(educational_level==3){
                    textViewJSON9.setText("Doctor");
                }
                //textViewJSON9.setText(educational_level);
                textViewJSON10.setText(age);
                if(gender==0) {
                    textViewJSON11.setText("male");
                }
                else if(gender==1){
                    textViewJSON11.setText("female");
                }
                textViewJSON12.setText(rate);
                if(marital_status==0) {
                    textViewJSON13.setText("Single");
                }
                else if(marital_status==1){
                    textViewJSON13.setText("Married");
                }
                else if(marital_status==2){
                    textViewJSON13.setText("Divorced");
                }
                else if(marital_status==3){
                    textViewJSON13.setText("Widow");
                }


                if(is_smoker==0) {
                    textViewJSON14.setText("Not Smoker");
                }
                else if(is_smoker==1){
                    textViewJSON14.setText("Smoker");
                }

            } catch (JSONException e) {
                e.printStackTrace();
                 Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }
}
