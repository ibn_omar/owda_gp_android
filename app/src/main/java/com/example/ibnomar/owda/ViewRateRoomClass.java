package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by islam on 28/06/17.
 */

public class ViewRateRoomClass  extends AsyncTask<String,Void,String> {
    private TextView textViewJSON1;
    private TextView textViewJSON2;
    private TextView textViewJSON3;
    private TextView textViewJSON4;
    private TextView textViewJSON5;
    private TextView textViewJSON6;
    private TextView textViewJSON7;
    private TextView textViewJSON8;

    private Context context;
    ProgressDialog loading;
    public String reus;
    private TextView textViewJSON;
    String Themail;
    JSONObject jsonobject;
    JSONArray jsonarray;
    String load;
    String room ;



    public  ViewRateRoomClass(Context context, TextView Rate ,TextView Bed,TextView Available,TextView Use,TextView Bath,TextView Balcon,TextView From,TextView To) {
        this.context = context;
        this.textViewJSON1 = Rate;
        this.textViewJSON2 = Bed;
        this.textViewJSON3 = Available;
        this.textViewJSON4 = Use;
        this.textViewJSON5 = Bath;
        this.textViewJSON6 = Balcon;
        this.textViewJSON7 = From;
        this.textViewJSON8 = To;
        //this.textViewJSON14 = textViewJSON14;

        //     myAdapter3=new MyAdapter3(context);

    }

    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }


    @Override
    protected String doInBackground(String... arg0) {
        //String user_id = arg0[0];
        String room_id = arg0[0];


        //Themail =id;
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            //data = "user_id=" + URLEncoder.encode(user_id, "UTF-8");
            data = "id=" + URLEncoder.encode(room_id, "UTF-8");

            URL url = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            //String parameters = "email="+email+"&password="+password;

            //url = new URL("http://owda.esy.es/owda/backend/user/get-user-by-id");
            url = new URL("http://owda.esy.es/owda/backend/room/get-room-by-id");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();

            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;


        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
        this.textViewJSON1.setText(result);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String room_id = jsonObj.getString("id");
                String rate = jsonObj.getString("rate");
                String beds = jsonObj.getString("num_of_beds");
                String available = jsonObj.getString("available_beds");
                String use = jsonObj.getString("used_as");
                int bath = jsonObj.getInt("has_bathroom");
                int Balcony = jsonObj.getInt("has_balcony");
                String fromDate = jsonObj.getString("available_from");
                String toDate = jsonObj.getString("available_to");
                String substrFrom=fromDate.substring(0,10);
                String substrTo=toDate.substring(0,10);
                room = room_id ;
                textViewJSON1.setText(rate);
                textViewJSON2.setText(beds);
                textViewJSON3.setText(available);
                textViewJSON4.setText(use);
                if(bath==1){
                    textViewJSON5.setText("Yes");

                }
                else{
                    textViewJSON5.setText("No");
                }
                if(Balcony==1){
                    textViewJSON6.setText("Yes");

                }
                else{
                    textViewJSON6.setText("No");
                }
               // textViewJSON5.setText(bath);
               // textViewJSON6.setText(Balcony);
                textViewJSON7.setText(substrFrom);
                textViewJSON8.setText(substrTo);





            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }
}
