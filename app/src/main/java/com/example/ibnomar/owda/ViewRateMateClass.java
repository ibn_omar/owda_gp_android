package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by islam on 29/06/17.
 */

public class ViewRateMateClass extends AsyncTask<String,Void,String> {

    private Context context;
    ProgressDialog loading;
    public String reus;
    private TextView textViewJSON1;
    private TextView textViewJSON2;
    private TextView textViewJSON3;
    private TextView textViewJSON4;;
    private TextView textViewJSON6;
    private TextView textViewJSON7;
    private TextView textViewJSON8;
    private TextView textViewJSON9;
    private TextView textViewJSON10;
    private TextView textViewJSON11;
    private TextView textViewJSON12;
    private TextView textViewJSON13;
    private TextView textViewJSON14;
    String user ;
    //  MyAdapter3 myAdapter3;



    public ViewRateMateClass(Context context, TextView rate ,TextView first,TextView last,TextView user,TextView email,TextView face,TextView phone,TextView nationalitty,TextView age,TextView edu,TextView gender,TextView mstatus,TextView smoker) {
        this.context = context;
        this.textViewJSON1 = rate;
        this.textViewJSON2 = first;
        this.textViewJSON3 = last;
        this.textViewJSON4 = user;
        this.textViewJSON6 = email;
        this.textViewJSON7 = face;
        this.textViewJSON8 = phone;
        this.textViewJSON9 = nationalitty;
        this.textViewJSON10 = age;
        this.textViewJSON11 = edu;
        this.textViewJSON12 = gender;
        this.textViewJSON13 = mstatus;
        this.textViewJSON14 = smoker;




        //     myAdapter3=new MyAdapter3(context);

    }


    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }


    @Override
    protected String doInBackground(String... arg0) {
        String user_id = arg0[0];


        //Themail =id;
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "id=" + URLEncoder.encode(user_id, "UTF-8");

            URL url = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            //String parameters = "email="+email+"&password="+password;

            //url = new URL("http://owda.esy.es/owda/backend/user/get-user-by-id");
            url = new URL("http://owda.esy.es/owda/backend/user/get-user");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();

            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;


        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
        //this.textViewJSON1.setText(result);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String user_id = jsonObj.getString("id");
                //String picture = jsonObj.getString("picture");

                String fname = jsonObj.getString("fname");
                String lname = jsonObj.getString("lname");
                String username = jsonObj.getString("username");
                String email = jsonObj.getString("email");
                String fb_account = jsonObj.getString("fb_account");
                String phone = jsonObj.getString("phone");
                String Nationality = jsonObj.getString("Nationality");
                int educational_level = jsonObj.getInt("educational_level");
                // int educational_level= jsonObj.getInt("educational_level");
                String age = jsonObj.getString("age");
                //String gender = jsonObj.getString("gender");
                int gender= jsonObj.getInt("gender");
                String rate = jsonObj.getString("rate");
                //String marital_status = jsonObj.getString("marital_status");
                int marital_status= jsonObj.getInt("marital_status");
                //String is_smoker = jsonObj.getString("is_smoker");
                int is_smoker= jsonObj.getInt("is_smoker");
                textViewJSON1.setText(rate);
                textViewJSON2.setText(fname);
                textViewJSON3.setText(lname);
                textViewJSON4.setText(username);
                textViewJSON6.setText(email);
                user=user_id;
                //textViewJSON5.setText(password);
                if(fb_account=="null"){
                    textViewJSON7.setText("Not Avaliable");

                }
                else{
                    textViewJSON7.setText(fb_account);
                }
                textViewJSON8.setText(phone);
                textViewJSON9.setText(Nationality);
                textViewJSON10.setText(age);
                if(educational_level==1){
                    textViewJSON11.setText("Student");
                }
                else if(educational_level==2){
                    textViewJSON11.setText("Bachelor");
                }
                else if(educational_level==3){
                    textViewJSON11.setText("Doctor");
                }
                //textViewJSON9.setText(educational_level);

                if(gender==0) {
                    textViewJSON12.setText("male");
                }
                else if(gender==1){
                    textViewJSON12.setText("female");
                }

                if(marital_status==0) {
                    textViewJSON13.setText("Single");
                }
                else if(marital_status==1){
                    textViewJSON13.setText("Married");
                }
                else if(marital_status==2){
                    textViewJSON13.setText("Divorced");
                }
                else if(marital_status==3){
                    textViewJSON13.setText("Widow");
                }


                if(is_smoker==0) {
                    textViewJSON14.setText("Not Smoker");
                }
                else if(is_smoker==1){
                    textViewJSON14.setText("Smoker");
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }
}
