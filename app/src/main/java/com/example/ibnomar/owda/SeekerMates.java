package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.net.URLEncoder;

/**
 * Created by islam on 23/06/17.
 */

public class SeekerMates extends AsyncTask<String,Void,String> {
    private Context context;
    ProgressDialog loading;
    public String reus;
    private TextView textViewJSON;
    String Themail;
    JSONObject jsonobject;
    JSONArray jsonarray;
    String load;
    ProgressDialog mProgressDialog;
    ArrayList<String> worldlist;
    String user_id;

    public SeekerMates(Context context) {
        this.context = context;

    }
    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }


    @Override
    protected String doInBackground(String... arg0) {
        user_id = arg0[0];
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "user_id=" + URLEncoder.encode(user_id, "UTF-8");

            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            //String parameters = "email="+email+"&password="+password;

            url = new URL("http://owda.esy.es/owda/backend/user/user-mates");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();

            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;


        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }

    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
//
        //this.textViewJSON.setText(result);

        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                jsonarray = jsonObj.getJSONArray("mates");
                String firstname []=new String[jsonarray.length()];
                String lastname []=new String[jsonarray.length()];
                String mate_id []=new String[jsonarray.length()];

                if (jsonarray.length() > 0){
                    for (int i = 0; i < jsonarray.length(); i++) {
                        jsonObj = jsonarray.getJSONObject(i);
                        String fname = jsonObj.getString("fname");
                        String lname = jsonObj.getString("username");
                        String id = jsonObj.getString("id");
                        firstname[i]=fname;
                        lastname[i]=lname;
                        mate_id[i]=id;
                        Toast.makeText(context, "Load Mates successfull.", Toast.LENGTH_SHORT).show();
                        Intent myIntent = new Intent(context, SeekerMatesList.class);
                        //myIntent.putExtra("Country", Country);
                        myIntent.putExtra("firstname", firstname);
                        myIntent.putExtra("lastname", lastname);
                        myIntent.putExtra("user_id", user_id);//
                        myIntent.putExtra("mate_id", mate_id);//
                        context.startActivity(myIntent);
                }



                }
                else
                {
                    Toast.makeText(context, "You don't have any Mates.", Toast.LENGTH_SHORT).show();
                }

                /*String logged = jsonObj.getString("logged");
                if (logged.equals("0")) {*/




                /*} else {
                    Toast.makeText(context, "Logout failed.", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(context,Themail , Toast.LENGTH_SHORT).show();

                }*/
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }

}
