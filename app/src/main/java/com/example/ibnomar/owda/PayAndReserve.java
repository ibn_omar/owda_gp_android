package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by fox on 24/06/17.
 */

public class PayAndReserve  extends AsyncTask<String,Void,String> {
    private Context context;
    ProgressDialog loading;
    String theMail;
    String user_id;
    String room_id;
    JSONObject jsonobject;
    JSONArray ads;
    JSONArray advertisements;
    public String reus;
    private TextView textViewJSON;

    public PayAndReserve(Context context) {

        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }



    @Override
    protected String doInBackground(String... arg0) {
        user_id= arg0[0];
        room_id = arg0[1];

        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "RoomUsers[user_id]=" + URLEncoder.encode(user_id, "UTF-8");
            data += "&RoomUsers[room_id]=" + URLEncoder.encode(room_id, "UTF-8");



            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            url = new URL("http://owda.esy.es/owda/backend/room/reserve-room");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();
            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;

        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;

        //this.textViewJSON.setText(result);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);






                    String error = jsonObj.getString("error");
                    String u_id = jsonObj.getString("user_id");
                    String r_id = jsonObj.getString("room_id");
                    String error_message=jsonObj.getString("error_msg");


                if(error.equals("0")) {

                    Toast.makeText(context, "Reserve Successfully.", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context, seeker.class);
                    myIntent.putExtra("id", user_id);
                    /*myIntent.putExtra("ads_title", ads_title);
                    myIntent.putExtra("ads_price", ads_price);
                    myIntent.putExtra("ads_id", ads_id);
                    myIntent.putExtra("room_id", room_id);
                    myIntent.putExtra("user_id", user_id);*/
                    context.startActivity(myIntent);
                }
                else {
                    //Toast.makeText(context, "Has been reserved.", Toast.LENGTH_SHORT).show();
                    Toast.makeText(context, error_message, Toast.LENGTH_SHORT).show();

                }
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }

    }
}
