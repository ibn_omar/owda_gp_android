package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by islam on 16/06/17.
 */

public class ViewAdClass extends AsyncTask<String,Void,String> {
    private TextView textViewJSON1;
    private TextView textViewJSON2;
    private TextView textViewJSON3;
    private TextView textViewJSON4;
    private TextView textViewJSON5;
    private TextView textViewJSON6;
    private TextView textViewJSON7;
   ;
    private Context context;
    ProgressDialog loading;
    public String reus;
    private TextView textViewJSON;
    String Themail;
    JSONObject jsonobject;
    JSONArray jsonarray;
    String load;
    ProgressDialog mProgressDialog;
    ArrayList<String> worldlist;
    String adv_id ;
    //  MyAdapter3 myAdapter3;



    public  ViewAdClass(Context context, TextView Title1 ,TextView body1,TextView price1,TextView inadv1,TextView Maintain1,TextView ExDate) {
        this.context = context;
        this.textViewJSON1 = Title1;
        this.textViewJSON2 = body1;
        this.textViewJSON3 = price1;
        this.textViewJSON4 = inadv1;
        this.textViewJSON5 = Maintain1;
        this.textViewJSON6 = ExDate;

        //this.textViewJSON14 = textViewJSON14;

        //     myAdapter3=new MyAdapter3(context);

    }

    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }


    @Override
    protected String doInBackground(String... arg0) {
        String id = arg0[0];


        //Themail =id;
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "add_id=" + URLEncoder.encode(id, "UTF-8");

            URL url = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            //String parameters = "email="+email+"&password="+password;

            //url = new URL("http://owda.esy.es/owda/backend/user/get-user-by-id");
            url = new URL("http://owda.esy.es/owda/backend/advertisement/edit-advert");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();

            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;


        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
        //this.textViewJSON1.setText(result);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String adver_id = jsonObj.getString("id");
                String Title1 = jsonObj.getString("title");
                String body1 = jsonObj.getString("body");
                String price1 = jsonObj.getString("price");
                int inadv1 = jsonObj.getInt("inadvance_pay");
                int Maintain1 = jsonObj.getInt("including_maintenance");
                String ExDate = jsonObj.getString("expire_date");
                adv_id = adver_id ;
                textViewJSON1.setText(Title1);
                textViewJSON2.setText(body1);
                textViewJSON3.setText(price1);
                if(inadv1==1){
                    textViewJSON4.setText("Yes");

                }
                else{
                    textViewJSON4.setText("No");
                }
                if(Maintain1==1){
                    textViewJSON5.setText("Yes");

                }
                else{
                    textViewJSON5.setText("No");
                }
                //textViewJSON4.setText(inadv1);
                //textViewJSON5.setText(Maintain1);
                textViewJSON6.setText(ExDate);





            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }
}
