package com.example.ibnomar.owda;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ResetPassword extends AppCompatActivity {
    protected EditText email;
    private EditText phone;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);


        email = (EditText) findViewById(R.id.email);
        phone = (EditText) findViewById(R.id.phone);

        Button reset = (Button) findViewById(R.id.reset);

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String E = email.getText().toString();
                String P = phone.getText().toString();


                Reset(v,E,P);
            }
        });


    }


    public void Reset(View v ,String Email,String Phone) {

        Boolean t = isNetworkConnected();
        if (!Email.equals("") && !Phone.equals(""))
        {
            if (t == true) {

                new SubmitReset(this).execute(Email, Phone);
            }
            else {
                //test2.setText("Connection error");
                // Toast.makeText(context, "Connection error", Toast.LENGTH_SHORT).show();
                Context context = getApplicationContext();
                CharSequence text = "Connection error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();


            }


        }
        else
        {
            if (email.getText().toString().length() == 0) {
                email.setError("Email is Required");
                email.requestFocus();
            }
            if (phone.getText().toString().length() == 0) {
                phone.setError("Phone not entered");
                phone.requestFocus();
            }

        }
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
