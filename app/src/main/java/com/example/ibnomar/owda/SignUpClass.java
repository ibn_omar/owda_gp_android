package com.example.ibnomar.owda;

/**
 * Created by islam on 12/05/17.
 */
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.io.OutputStreamWriter;

public class SignUpClass extends AsyncTask<String,Void,String>{
    private Context context;
    ProgressDialog loading;
    String theMail;
    public String reus;
    private TextView textViewJSON;

    public SignUpClass(Context context, TextView textViewJSON) {

        this.context = context;
        this.textViewJSON=textViewJSON;
    }

    @Override
    protected void onPreExecute() {
        loading=  ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }




    @Override
    protected String doInBackground(String... arg0) {
        String fname = arg0[0];
        String lname = arg0[1];
        String picture=arg0[2];
        String username = arg0[3];
        String password = arg0[4];
        String phone = arg0[5];
        String email = arg0[6];
        String Nationality = arg0[7];
        String fb_account = arg0[8];
        String educational_level = arg0[9];
        String age = arg0[10];
        String gender = arg0[11];
        String rate=arg0[13];
        String marital_status = arg0[13];
        String is_smoker = arg0[14];


        theMail = email;
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "User[fname]=" + URLEncoder.encode(fname, "UTF-8");
            data += "&User[lname]=" + URLEncoder.encode(lname, "UTF-8");
            data += "&User[picture]=" + URLEncoder.encode(picture, "UTF-8");
            data += "&User[username]=" + URLEncoder.encode(username, "UTF-8");
            data += "&User[password]=" + URLEncoder.encode(password, "UTF-8");
            data += "&User[phone]=" + URLEncoder.encode(phone, "UTF-8");
            data += "&User[email]=" + URLEncoder.encode(email, "UTF-8");
            data += "&User[Nationality]=" + URLEncoder.encode(Nationality, "UTF-8");
            data += "&User[fb_account]=" + URLEncoder.encode(fb_account, "UTF-8");
            data += "&User[educational_level]=" + URLEncoder.encode(educational_level, "UTF-8");
            data += "&User[age]=" + URLEncoder.encode(age, "UTF-8");
            data += "&User[gender]=" + URLEncoder.encode(gender, "UTF-8");
            data += "&User[rate]=" + URLEncoder.encode("4", "UTF-8");
            data += "&User[marital_status]=" + URLEncoder.encode(marital_status, "UTF-8");
            data += "&User[is_smoker]=" + URLEncoder.encode(is_smoker, "UTF-8");


            //   link = "http://10.0.2.2:8080/LoginGP.php" + data;
            //  link = "http://10.0.2.2:8012/LoginGP.php" + data;
            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            url = new URL("http://owda.esy.es/owda/backend/user/add-user");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();
            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;

        } catch (Exception e) {
            //Toast.makeText(context, "This User Name or Email or Phone Is already taken", Toast.LENGTH_SHORT).show();

            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
        if(result.equals(""))
        {
            Toast.makeText(context, "This Email or Phone Is already taken", Toast.LENGTH_SHORT).show();
        }



       // this.textViewJSON.setText(result);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String id = jsonObj.getString("user_id");
                String error = jsonObj.getString("error");
                if (error.equals("0")) {
                    Toast.makeText(context, "Data inserted successfully. Signup completed successfully.", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context, signin.class);
                    myIntent.putExtra("id", id);
                    context.startActivity(myIntent);
                } else if (error.equals("1")) {
                    String error_msg=jsonObj.getString("error_msg");
                    Toast.makeText(context, error_msg, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(context,Themail , Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(context, "Couldn't connect to remote database.", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }

}
