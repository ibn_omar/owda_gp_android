package com.example.ibnomar.owda;

import android.support.v7.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.app.Activity;
import android.os.Bundle;
import android.widget.*;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.view.LayoutInflater;
import android.app.ListActivity;
import android.os.Bundle;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import android.content.ContentUris;
import android.net.Uri;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.os.Bundle;
import android.widget.Toast;
import android.os.Bundle;

public class MyMessagesListView extends AppCompatActivity {
    String message_id [];
    String from_user [];
    String subject [];
    String user_id;
    String user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_messages_list_view);
        ListView ListView = (ListView)findViewById(R.id.mymessageListView);
        Bundle extras = getIntent().getExtras();
        message_id = extras.getStringArray("message_id");
        from_user = extras.getStringArray("from_user");
        subject = extras.getStringArray("subject");
        user_id = extras.getString("user_id");
        user=extras.getString("user");
        MyMessagesListView.CustomAdapter customAdapter = new MyMessagesListView.CustomAdapter();
        ListView.setAdapter(customAdapter);
        ListView.setClickable(true);
        ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Intent i = new Intent(getApplicationContext(),viewMessage.class);
                i.putExtra("message_id",message_id[position]);
                i.putExtra("user_id",user_id);
                i.putExtra("user",user);
                startActivity(i);
            }
        });



    }

    class CustomAdapter extends BaseAdapter {


        @Override
        public int getCount() {
            return message_id.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.custommymessagelist,null);

            TextView subjectt = (TextView)view.findViewById(R.id.subject);
            TextView fromUserr = (TextView)view.findViewById(R.id.fromUser);

            subjectt.setText(subject[i]);
            fromUserr.setText(from_user[i]);


            return view;

        }

    }
}
