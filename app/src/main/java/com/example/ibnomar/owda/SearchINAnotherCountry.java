package com.example.ibnomar.owda;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class SearchINAnotherCountry extends AppCompatActivity {
Button search;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_inanother_country);

        search = (Button) findViewById(R.id.search);
        final Spinner spin = (Spinner) findViewById(R.id.spinner);
        final String Country_id [];
        final String Country_name [];
        final String user_id ;
        Bundle extras = getIntent().getExtras();
        Country_id = extras.getStringArray("Country_id");
        Country_name = extras.getStringArray("Country_name");
        user_id = extras.getString("user_id");
        Spinner mySpinner = (Spinner) findViewById(R.id.spinner);
        mySpinner.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,
                Country_name));

        search.setOnClickListener(new View.OnClickListener() {

            Boolean t = isNetworkConnected();

            @Override
            public void onClick(View v) {
                String Sp = spin.getSelectedItem().toString();
                int Spinindex=spin.getSelectedItemPosition();
                String C_id=Country_id[Spinindex];

                SearchData(v,user_id ,C_id);

            }

        });
    }
    public void SearchData(View v,String user_id,String country_id ) {

        Boolean t = isNetworkConnected();

            if (t == true) {
                new LoadCityINCountry(this).execute(user_id,country_id);


            } else {
                //test.setText("Network Failed");
                Context context = getApplicationContext();
                CharSequence text = "Connection error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

            }


    }

    private boolean isNetworkConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
