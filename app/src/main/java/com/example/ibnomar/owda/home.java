package com.example.ibnomar.owda;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;


public class home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Button button1 = (Button) findViewById(R.id.signin);
        Button button2 = (Button) findViewById(R.id.signup);


        button1.setOnClickListener(new OnClickListener(){
            public void onClick(View arg0)
            {   Intent i = new Intent(getApplicationContext(),signin.class);
                startActivity(i);

            }   });
       /* button2.setOnClickListener(new OnClickListener(){
            public void onClick(View arg0)
            {   Intent i = new Intent(getApplicationContext(),sign_up1.class);
                startActivity(i);

            }   });*/
        button2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               Country(v);

            }
        });
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    public void Country(View v){
        Boolean t = isNetworkConnected();
        if (t == true){
            new LoadCountries(this).execute("0");
            //Intent i = new Intent(getApplicationContext(),sign_up1.class);
            //startActivity(i);
        }
        else{
            Context context = getApplicationContext();
            CharSequence text = "Connection error";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }
}
