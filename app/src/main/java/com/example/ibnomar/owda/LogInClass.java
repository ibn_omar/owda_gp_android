package com.example.ibnomar.owda;


import org.apache.http.client.HttpClient;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONStringer;
import org.json.JSONTokener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.io.OutputStreamWriter;

/**
 * Created by BoDy on 23/07/2016.
 */
public class LogInClass extends AsyncTask<String,Void,String> {

    private Context context;
    ProgressDialog loading;
    public String reus;
    private TextView textViewJSON;
    String Themail;
    public static String gloable_id;


  //  MyAdapter3 myAdapter3;



    public LogInClass(Context context, TextView textViewJSON) {
        this.context = context;
        this.textViewJSON = textViewJSON;
   //     myAdapter3=new MyAdapter3(context);

    }

    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }


    @Override
    protected String doInBackground(String... arg0) {
        String email = arg0[1];
        String password = arg0[0];


        Themail =email;
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "email=" + URLEncoder.encode(email, "UTF-8");
            data += "&password=" + URLEncoder.encode(password, "UTF-8");
            // data = "?email="+email+"&password="+password;
            //   link = "http://10.0.2.2:8080/LoginGP.php" + data;
            //  link = "http://10.0.2.2:8012/LoginGP.php" + data;
       //     link = "http://bodytop2015.comli.com/LoginStock.php" + data;
           // link = "http://owda.esy.es/owda/backend/user/login" + data;
           //http://owda.esy.es/owda/backend/user/get-user-by-id?id=1 link = "http://owda.esy.es/owda/backend/user/login";
            //link ="http://owda.esy.es/owda/backend/user/login?email=ibn%40omar.com&password=123456789";


            /*URL url = new URL(link);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestMethod("POST");

            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            result = bufferedReader.readLine();
            return result;*/


            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            //String parameters = "email="+email+"&password="+password;

            url = new URL("http://owda.esy.es/owda/backend/user/login");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();
            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;


        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
//        if (result.equals("successfully"))
        /*if (result.equals("response")) {
            Intent myIntent = new Intent(context, user.class);
            myIntent.putExtra("email", Themail);
            context.startActivity(myIntent);
        }*/
       // this.textViewJSON.setText(result);

        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String id = jsonObj.getString("user_id");
                String error = jsonObj.getString("error");
                if (error.equals("0")) {
                    Toast.makeText(context, "Data inserted successfully. Sigin successfull.", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context, user.class);
                    myIntent.putExtra("id", id);
                    context.startActivity(myIntent);

                } else if (error.equals("1")) {
                    Toast.makeText(context, "Data could not be inserted. Logain failed.", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(context,Themail , Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(context, "Couldn't connect to remote database.", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                 Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }


}
