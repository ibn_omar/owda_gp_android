package com.example.ibnomar.owda;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.content.Context;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;


public class AddAdvertisemnet extends AppCompatActivity  {


    Button Submit;
    EditText NumOfBeds, AvailableBeds, RoomUsage, photo,AdsTitle,AdsDiscribtion, AdsPrice,StartDate,EndDate;
    TextView test1;
    RadioGroup HasBalcony,HasBath,InAdvancePay,includeMaintenance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_advertisemnet);
        //initialization of all editText
        NumOfBeds = (EditText) findViewById(R.id.NumOfBeds);
        AvailableBeds = (EditText) findViewById(R.id.AvailableBeds);
        RoomUsage = (EditText) findViewById(R.id.RoomUsage);
        //photo = (EditText) findViewById(R.id.);
        //Initialization of Register Button
        Submit = (Button) findViewById(R.id.Submit);
        test1 = (TextView) findViewById(R.id.test1);
        AdsTitle=(EditText)findViewById(R.id.AdTitle);
        AdsDiscribtion=(EditText)findViewById(R.id.AdDiscribtion);
        AdsPrice=(EditText)findViewById(R.id.AdPrice);

        HasBalcony=(RadioGroup)findViewById(R.id.HasBalcony);
        HasBath=(RadioGroup)findViewById(R.id.HasBath);
        InAdvancePay=(RadioGroup)findViewById(R.id.InAdvancePay);
        includeMaintenance=(RadioGroup)findViewById(R.id.includeMaintenance);
        StartDate=(EditText)findViewById(R.id.StartDate);
        EndDate=(EditText)findViewById(R.id.EndDate);
        Bundle extras = getIntent().getExtras();
        final String id = extras.getString("id");

        Submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //int hB =HasBalcony.getCheckedRadioButtonId();
                int radioButtonID1 = HasBalcony.getCheckedRadioButtonId();
                View radioButton1 = HasBalcony.findViewById(radioButtonID1);
                int hB = HasBalcony.indexOfChild(radioButton1);


                int radioButtonID2 = HasBath.getCheckedRadioButtonId();
                View radioButton2 = HasBath.findViewById(radioButtonID2);
                int hBT = HasBath.indexOfChild(radioButton2);

                //int hBT =HasBath.getCheckedRadioButtonId();



                int radioButtonID3 = InAdvancePay.getCheckedRadioButtonId();
                View radioButton3 = InAdvancePay.findViewById(radioButtonID3);
                int iAD = InAdvancePay.indexOfChild(radioButton3);
                //int iAD =InAdvancePay.getCheckedRadioButtonId();


                int radioButtonID4 = includeMaintenance.getCheckedRadioButtonId();
                View radioButton4 = includeMaintenance.findViewById(radioButtonID4);
                int iM = includeMaintenance.indexOfChild(radioButton4);
               // int iM =includeMaintenance.getCheckedRadioButtonId();
                //rSex = (RadioButton) findViewById(selectedId1);
                //rStatus = (RadioButton) findViewById(selectedId2);
                //rSmoke = (RadioButton) findViewById(selectedId3);
                String nB=NumOfBeds.getText().toString();
                String nAB=AvailableBeds.getText().toString();
                String rU=RoomUsage.getText().toString();
                String aT=AdsTitle.getText().toString();
                String aD=AdsDiscribtion.getText().toString();
                String aP=AdsPrice.getText().toString();
                String sD=StartDate.getText().toString();
                String eD=EndDate.getText().toString();
                //String reserved="1";
                String pic="null";
                //String rt="4";

                AddAdv(v,nB,nAB,rU,hBT,hB/*rt,reserved*/,sD,eD,aT,aD,aP,iAD,iM,id,pic);
            }
        });
    }

    public  void AddAdv(View v,String numBeds,String numAvailable,String roomUse,int Bath,int Balcon,/*String rateString Reserve*/String StartDate,String endDate,String AdTititle ,String AdBody,String AdPrice,int inAPay, int inMaintenence,String id,String pic) {

        Boolean t = isNetworkConnected();
        if (!numBeds.equals("") && !numAvailable.equals("") && !pic.equals("")  /* !Reserve.equals("") */&& !AdPrice.equals("") && !AdBody.equals("") && !AdTititle.equals("")&&!endDate.equals("")) {
            if (t == true) {
                new AddAvertisementClass(this, test1).execute(numBeds, numAvailable,roomUse, String.valueOf(Bath), String.valueOf(Balcon)/*, rate Reserve*/, StartDate,endDate,AdTititle,AdBody,AdPrice, String.valueOf(inAPay), String.valueOf(inMaintenence),id,pic);
                if (test1.getText() == "successfully") {

                    /*Intent i = new Intent(getApplicationContext(),Login.class);
                    startActivity(i);
                    Intent secondActivity = new Intent(MainActivity.this,
                            LiveStream.class);
                    startActivity(secondActivity);*/
                }


            } else {
                test1.setText("Network Failed");

            }
        } else {

            /*Context context = getApplicationContext();
            CharSequence text = "Please Fill all Fields";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();*/
            if (NumOfBeds.getText().toString().length() == 0) {
                NumOfBeds.setError("Number of Beds Field is Required");
                NumOfBeds.requestFocus();
            } else if (AvailableBeds.getText().toString().length() == 0) {
                AvailableBeds.setError("Number of Available Beds Field is Required");
                AvailableBeds.requestFocus();
            } else if (AdsPrice.getText().toString().length() == 0) {
                AdsPrice.setError("Please confirm password");
                AdsPrice.requestFocus();
            }
            else if (AdsDiscribtion.getText().toString().length() == 0) {
                AdsDiscribtion.setError("Advertisement's Body is required");
                AdsDiscribtion.requestFocus();


            } else if (AdsPrice.getText().toString().length() == 0) {
                AdsPrice.setError("Advertisement's Price Is Required");
                AdsPrice.requestFocus();
            } else if (AdsTitle.getText().toString().length() == 0) {
                AdsTitle.setError("Advertisement's Title Is Required");
                AdsTitle.requestFocus();
            }
            else if (EndDate.getText().toString().length() == 0) {
                EndDate.setError("Room's Availability Is Required");
                EndDate.requestFocus();
            }

        }
    }
    private boolean isNetworkConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}