package com.example.ibnomar.owda;

/**
 * Created by islam on 12/05/17.
 */
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.io.OutputStreamWriter;

public class ApartmentClass extends AsyncTask<String,Void,String>{
    private Context context;
    ProgressDialog loading;
    String theID;
    public String reus;
    private TextView textViewJSON;

    public ApartmentClass(Context context) {

        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        loading=  ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }




    @Override
    protected String doInBackground(String... arg0) {

        String user_id = arg0[0];
        String address = arg0[1];
        String metro = arg0[2];
        String floorNum = arg0[3];
        String NumRooms = arg0[4];
        String maxhabit = arg0[5];
        String currenthabit = arg0[6];
        String separate_bathroomsradio1 = arg0[7];
        String has_kitchenradio2 = arg0[8];
        String has_wifiradio3 = arg0[9];
        String furnitureradio4 = arg0[10];
        String pets_allowanceradio5 = arg0[11];
        String genderradio6 = arg0[12];
        String elvetorradio7 = arg0[13];
        String smokeradio8 = arg0[14];
        String ownerradio9 = arg0[15];
        String city_id = arg0[16];





        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "user_id=" + URLEncoder.encode(user_id, "UTF-8");
            data += "&Apartments[ownership_type]=" + URLEncoder.encode(ownerradio9, "UTF-8");
            data += "&Apartments[city_id]=" + URLEncoder.encode(city_id, "UTF-8");
            data += "&Apartments[address]=" + URLEncoder.encode(address, "UTF-8");
            data += "&Apartments[nearist_metro]=" + URLEncoder.encode(metro, "UTF-8");
            data += "&Apartments[floor_num]=" + URLEncoder.encode(floorNum, "UTF-8");
            data += "&Apartments[num_of_rooms]=" + URLEncoder.encode(NumRooms, "UTF-8");
            data += "&Apartments[max_inhabitants]=" + URLEncoder.encode(maxhabit, "UTF-8");
            data += "&Apartments[current_inhabitants]=" + URLEncoder.encode(currenthabit, "UTF-8");
            data += "&Apartments[gender_preferences]=" + URLEncoder.encode(genderradio6, "UTF-8");
            data += "&Apartments[has_kitchen]=" + URLEncoder.encode(has_kitchenradio2, "UTF-8");
            data += "&Apartments[separate_bathrooms]=" + URLEncoder.encode(separate_bathroomsradio1, "UTF-8");
            data += "&Apartments[has_elevator]=" + URLEncoder.encode(elvetorradio7, "UTF-8");
            data += "&Apartments[pets_allowance]=" + URLEncoder.encode(pets_allowanceradio5, "UTF-8");
            data += "&Apartments[smoking_allowance]=" + URLEncoder.encode(smokeradio8, "UTF-8");
            data += "&Apartments[has_wifi]=" + URLEncoder.encode(has_wifiradio3, "UTF-8");
            data += "&Apartments[furniture_status]=" + URLEncoder.encode(furnitureradio4, "UTF-8");




            //   link = "http://10.0.2.2:8080/LoginGP.php" + data;
            //  link = "http://10.0.2.2:8012/LoginGP.php" + data;
            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            url = new URL("http://owda.esy.es/owda/backend/apartment/add-apartment");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();
            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;

        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
        /*if(result.equals("successfully"))
        {

            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
        }
        else{

            Toast.makeText(context, "This Email Is already taken", Toast.LENGTH_SHORT).show();
        }

*/
        //this.textViewJSON.setText(result);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String id = jsonObj.getString("user_id");
                String error = jsonObj.getString("error");

                if (error.equals("0")) {
                    Toast.makeText(context, "Now You Have Apartment.", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context, AddAdvertisemnet.class);
                    myIntent.putExtra("id", id);
                    context.startActivity(myIntent);
                } else if (error.equals("1")) {
                    String error_message = jsonObj.getString("error_message");
                    //Toast.makeText(context, "Data could not be inserted. Signup failed.", Toast.LENGTH_SHORT).show();
                    //Context context = getApplicationContext();
                    String text = error_message;
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();


                } else {
                    Toast.makeText(context, "Couldn't connect to remote database.", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }

}
