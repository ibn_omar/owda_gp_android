package com.example.ibnomar.owda;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.app.Activity;
import android.os.Bundle;
import android.widget.*;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.view.LayoutInflater;
import android.app.ListActivity;
import android.os.Bundle;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import android.content.ContentUris;
import android.net.Uri;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.os.Bundle;
import android.widget.Toast;

public class AdvListView extends AppCompatActivity {
    String ads_id [];
    String ads_title [];
    String ads_price [];
    String room_id [];
    String user_id;
    String Adv_id;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adv_list_view);
        ListView ListView = (ListView)findViewById(R.id.listView1);
        Bundle extras = getIntent().getExtras();
        ads_id = extras.getStringArray("ads_id");
        ads_title = extras.getStringArray("ads_title");
        ads_price = extras.getStringArray("ads_price");
        room_id = extras.getStringArray("room_id");
        user_id = extras.getString("user_id");
        CustomAdapter customAdapter = new CustomAdapter();
        ListView.setAdapter(customAdapter);

        ListView.setClickable(true);
        ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Intent i = new Intent(getApplicationContext(),ViewSeekerAd.class);
                i.putExtra("Adv_id",ads_id[position]);
                i.putExtra("room_id",room_id[position]);
                i.putExtra("user_id",user_id);
                startActivity(i);
            }
        });
    }
    class CustomAdapter extends BaseAdapter{


        @Override
        public int getCount() {
            return ads_id.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.customlistview,null);

            TextView textView_name1 = (TextView)view.findViewById(R.id.textView1);
            TextView textView_name2 = (TextView)view.findViewById(R.id.textView2);

            textView_name1.setText(ads_title[i]);
            textView_name2.setText(ads_price[i]);
            //Adv_id=ads_id[i];
            //button.setTag(textView_name1.getText().toString());


            return view;

        }

    }
    /*public void ChoseAds(View v ) {
        Boolean t = isNetworkConnected();

        if (t == true) {

            Intent i = new Intent(getApplicationContext(),user.class);
            //i.putExtra("id", id);
            startActivity(i);
        }
        else {
            //text.setText("Connection error");
            //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
            Context context = getApplicationContext();
            CharSequence text = "Connection error";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }

    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }*/
}
