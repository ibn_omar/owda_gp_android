package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by fox on 15/06/17.
 */

public class SubmitEditProfile  extends AsyncTask<String,Void,String> {
    private Context context;
    ProgressDialog loading;
    String theMail;
    public String reus;
    private TextView textViewJSON;
    String user;
    public SubmitEditProfile(Context context, TextView textViewJSON) {

        this.context = context;
        this.textViewJSON = textViewJSON;
    }

    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }



    @Override
    protected String doInBackground(String... arg0) {
        String username = arg0[0];
        String email = arg0[1];
        String password = arg0[2];
        String ConfPass = arg0[3];
        String fname = arg0[4];
        String lname = arg0[5];
        String age = arg0[6];
        String  phone= arg0[7];
        String fb_account = arg0[8];
        String gender = arg0[9];
        String marital_status = arg0[10];
        String is_smoker = arg0[11];
        String educational_level = arg0[12];
        String Nationality = arg0[13];
        String picture = arg0[14];
        String rate = arg0[15];
        String id = arg0[16];
         user = arg0[17];


        theMail = picture;
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "id=" + URLEncoder.encode(id, "UTF-8");
            data += "&User[username]=" + URLEncoder.encode(username, "UTF-8");
            data += "&User[email]=" + URLEncoder.encode(email, "UTF-8");
            data += "&User[password]=" + URLEncoder.encode(password, "UTF-8");
            data += "&User[fname]=" + URLEncoder.encode(fname, "UTF-8");
            data += "&User[lname]=" + URLEncoder.encode(lname, "UTF-8");
            data += "&User[age]=" + URLEncoder.encode(age, "UTF-8");
            data += "&User[phone]=" + URLEncoder.encode(phone, "UTF-8");
            data += "&User[fb_account]=" + URLEncoder.encode(fb_account, "UTF-8");
            data += "&User[gender]=" + URLEncoder.encode(gender, "UTF-8");
            data += "&User[marital_status]=" + URLEncoder.encode(marital_status, "UTF-8");
            data += "&User[is_smoker]=" + URLEncoder.encode(is_smoker, "UTF-8");
            data += "&User[educational_level]=" + URLEncoder.encode(educational_level, "UTF-8");
            data += "&User[Nationality]=" + URLEncoder.encode(Nationality, "UTF-8");
            data += "&User[picture]=" + URLEncoder.encode("null", "UTF-8");
            //data += "&User[rate]=" + URLEncoder.encode(rate, "UTF-8");

            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            url = new URL("http://owda.esy.es/owda/backend/user/edit-user");
            connection = (HttpURLConnection) url.openConnection();

            connection.setReadTimeout(15000);
            connection.setConnectTimeout(15000);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();
            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;

        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;

        //this.textViewJSON.setText(result);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String id = jsonObj.getString("user_id");
                String error = jsonObj.getString("error");


                if (error.equals("0")) {
                    Toast.makeText(context, "Updated successfully.", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context, MyProfile.class);
                    myIntent.putExtra("id", id);
                    myIntent.putExtra("user", user);

                    context.startActivity(myIntent);
                }
                else if (error.equals("1")) {
                    String error_msg=jsonObj.getString("error_msg");
                    Toast.makeText(context, error_msg, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(context,theMail , Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(context, "Couldn't connect to remote database.", Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }

    }
}
