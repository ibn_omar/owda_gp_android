package com.example.ibnomar.owda;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SearchByPriceCity extends AppCompatActivity {
    EditText editprice;
    Button search;
    TextView test;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_price_city);

        search = (Button) findViewById(R.id.search);
        editprice = (EditText) findViewById(R.id.price);
        final Spinner spin = (Spinner) findViewById(R.id.spinner);
        final String Country_id [];
        final String City_name [];
        final String City_id [];
        final String user_id ;
        Bundle extras = getIntent().getExtras();
        Country_id = extras.getStringArray("Country_id");
        City_name = extras.getStringArray("City_name");
        City_id = extras.getStringArray("City_id");
        user_id = extras.getString("user_id");
        Spinner mySpinner = (Spinner) findViewById(R.id.spinner);
        mySpinner.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,
                City_name));

        search.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String Sp = spin.getSelectedItem().toString();
                int Spinindex=spin.getSelectedItemPosition();
                String city_id=City_id[Spinindex];


                String pricee = editprice.getText().toString();

                SearchData(v,user_id ,city_id, pricee);
            }
        });
    }
    public void SearchData(View v,String user_id,String city_id ,String pricee ) {

        Boolean t = isNetworkConnected();

        if (!pricee.equals("")) {

            if (t == true) {
                new SubmitSearchPriceCity(this).execute(user_id,city_id, pricee);


            } else {
                //test.setText("Network Failed");
                Context context = getApplicationContext();
                CharSequence text = "Connection error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

            }

        } else {

            Context context = getApplicationContext();
            CharSequence text = "Please Add Price.";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();


        }
    }





    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}

