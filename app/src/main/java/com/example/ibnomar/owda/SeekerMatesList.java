package com.example.ibnomar.owda;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class SeekerMatesList extends AppCompatActivity {

    String firstname [];
    String lastname [];
    String mate_id [];
    String user_id;//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seeker_mates_list);
        ListView ListView = (ListView)findViewById(R.id.SeekerMatesList);
        Bundle extras = getIntent().getExtras();
        mate_id = extras.getStringArray("mate_id");
        firstname = extras.getStringArray("firstname");
        lastname = extras.getStringArray("lastname");
        user_id = extras.getString("user_id");//
        SeekerMatesList.CustomAdapter customAdapter = new SeekerMatesList.CustomAdapter();
        ListView.setAdapter(customAdapter);
        ListView.setClickable(true);
        ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Intent i = new Intent(getApplicationContext(),UserRate.class);
                i.putExtra("mate_id",mate_id[position]);
                i.putExtra("user_id",user_id);
                startActivity(i);
            }
        });

    }
    class CustomAdapter extends BaseAdapter {


        @Override
        public int getCount() {
            return mate_id.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.customseekermates,null);

            TextView textView_name1 = (TextView)view.findViewById(R.id.textfirst);
            TextView textView_name2 = (TextView)view.findViewById(R.id.textlast);

            textView_name1.setText(firstname[i]);
            textView_name2.setText(lastname[i]);
            //Adv_id=ads_id[i];
            //button.setTag(textView_name1.getText().toString());


            return view;

        }

    }
    @Override
    public void onBackPressed() {
        /*Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);*/
        Intent i = new Intent(getApplicationContext(),seeker.class);
        Bundle extras = getIntent().getExtras();
        String id = extras.getString("user_id");
        i.putExtra("id", id);
        startActivity(i);
        //finish();
    }
}
