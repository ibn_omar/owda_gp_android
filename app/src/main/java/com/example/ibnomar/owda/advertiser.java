package com.example.ibnomar.owda;

import android.app.LoaderManager;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class advertiser extends AppCompatActivity {

    Context context;
    TextView text;
    //String id;
    private DrawerLayout bDrawer;
    private ActionBarDrawerToggle bToggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        finish();
        Intent new_advr = new Intent(this,advertiserNavigation.class);
        Bundle extras = getIntent().getExtras();
        String id = extras.getString("id");
        new_advr.putExtra("user_id", id);
        startActivity(new_advr);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertiser);


        bDrawer=(DrawerLayout) findViewById(R.id.DrawerLayout);
        bToggle=new ActionBarDrawerToggle(this,bDrawer,R.string.open,R.string.close);

        bDrawer.addDrawerListener(bToggle);
        bToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        text = (TextView) findViewById(R.id.text);

        /*
        Button button1 = (Button) findViewById(R.id.ads);
        button1.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                Intent i = new Intent(getApplicationContext(), viewAdvertisement.class);
                startActivity(i);

            }
        });*/

        Button button = (Button) findViewById(R.id.ads);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Bundle extras = getIntent().getExtras();
                String id = extras.getString("id");

                Te(v,id);


            }

        });

        Button button3 = (Button) findViewById(R.id.advertise);
        button3.setOnClickListener(new View.OnClickListener() {

            Boolean t = isNetworkConnected();

            @Override
            public void onClick(View v) {
                /*if (t == true) {
                    Bundle extras = getIntent().getExtras();
                    String user_id = extras.getString("id");
                    new CheckApartment().execute(user_id);
                    Intent i = new Intent(getApplicationContext(), AddAdvertisemnet.class);
                Bundle extras = getIntent().getExtras();
                String user_id = extras.getString("id");
                i.putExtra("id", user_id);
                startActivity(i);
                }
                else {
                    //text.setText("Connection error");
                    //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
                    Context context = getApplicationContext();
                    CharSequence text = "Connection error";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                }*/
                Bundle extras = getIntent().getExtras();
                String id = extras.getString("id");

                CheckAp(v,id);

            }

        });




        Button button2 = (Button) findViewById(R.id.advprofile);

        button2.setOnClickListener(new View.OnClickListener() {

            Boolean t = isNetworkConnected();

                @Override
                public void onClick(View v) {
                    if (t == true) {
                        Intent i = new Intent(getApplicationContext(), MyProfile.class);
                        Bundle extras = getIntent().getExtras();
                        String id = extras.getString("id");
                        i.putExtra("id", id);
                        startActivity(i);
                    }
                    else {
                        //text.setText("Connection error");
                        //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
                        Context context = getApplicationContext();
                        CharSequence text = "Connection error";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();

                    }


            }

        });

        Button message = (Button) findViewById(R.id.advinbox);

        message.setOnClickListener(new View.OnClickListener() {

            Boolean t = isNetworkConnected();

            @Override
            public void onClick(View v) {
                if (t == true) {
                    Intent i = new Intent(getApplicationContext(), Messages.class);
                    Bundle extras = getIntent().getExtras();
                    String id = extras.getString("id");
                    i.putExtra("user_id", id);
                    startActivity(i);
                }
                else {
                    //text.setText("Connection error");
                    //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
                    Context context = getApplicationContext();
                    CharSequence text = "Connection error";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                }


            }

        });


        Button logout = (Button) findViewById(R.id.advlogout);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), advertiserNavigation.class);
                Bundle extras = getIntent().getExtras();
                String id = extras.getString("id");
                i.putExtra("user_id", id);
                startActivity(i);
              //  logout(v);
            }
        });
    }
    public void logout(View v ) {
        Boolean t = isNetworkConnected();

            if (t == true) {

                new Logout(this).execute();
            }
            else {
                //text.setText("Connection error");
                //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
                Context context = getApplicationContext();
                CharSequence text = "Connection error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

            }

    }

    public void Te(View v ,String id) {
        Boolean t = isNetworkConnected();

        if (t == true) {

            new AdvAds(this).execute(id);
        } else {
            //text.setText("Connection error");
            //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
            Context context = getApplicationContext();
            CharSequence text = "Connection error";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }
    }
    public void CheckAp(View v ,String id) {
        Boolean t = isNetworkConnected();

        if (t == true) {

            new CheckApartment(this,text).execute(id);
        } else {
            //text.setText("Connection error");
            //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
            Context context = getApplicationContext();
            CharSequence text = "Connection error";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(bToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        /*Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);*/
        Intent i = new Intent(getApplicationContext(),user.class);
        Bundle extras = getIntent().getExtras();
        String id = extras.getString("id");
        i.putExtra("id", id);
        startActivity(i);
        //finish();
    }

}