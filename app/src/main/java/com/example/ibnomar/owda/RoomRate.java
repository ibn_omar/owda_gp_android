package com.example.ibnomar.owda;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RoomRate extends AppCompatActivity {

    Button submitRate;
    EditText rateroom, comment;
    TextView rateview;
    TextView bed;
    TextView availBed;
    TextView used;
    TextView bath;
    TextView balacony;
    TextView todate;
    TextView fromdate;
    TextView test;
    TextView text2;
    Context context;
    String room_id ;
    String user_id ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_rate);
        submitRate = (Button) findViewById(R.id.SubmitRate);
        rateroom = (EditText) findViewById(R.id.YourRate);
        comment = (EditText) findViewById(R.id.CommentR);
        rateview = (TextView) findViewById(R.id.Roomrate);
        bed = (TextView) findViewById(R.id.beds1);
        availBed = (TextView) findViewById(R.id.availableBed);
        //beds1 = (TextView) findViewById(R.id.beds1);
        //text5 = (TextView) findViewById(R.id.text5);
        used = (TextView) findViewById(R.id.usedas);
        bath = (TextView) findViewById(R.id.hasbath);
        balacony = (TextView) findViewById(R.id.hasbalacony);
        fromdate = (TextView) findViewById(R.id.FromDateRoom);
        todate = (TextView) findViewById(R.id.ToDateRoom);
        Bundle extras = getIntent().getExtras();
        room_id = extras.getString("room_id");
        user_id = extras.getString("user_id");
        new ViewRateRoomClass(this,rateview,bed,availBed,used,bath,balacony,fromdate,todate).execute(room_id);
        submitRate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {





                String rr = rateroom.getText().toString();
                String rc = comment.getText().toString();
                String rid = room_id;
                String usrid = user_id ;

                RRate(v,rid , usrid ,rr,rc );
            }
        });
    }

    public void RRate(View v,  String roomid , String userid , String rate,String Comment ) {

        Boolean t = isNetworkConnected();
        if (!rate.equals("") && !Comment.equals("") ) {


        if(rate.equals("1") || rate.equals("2") || rate.equals("3") || rate.equals("4")) {

              if (t == true) {

                  new RoomRateClass(this, test).execute(roomid, userid, rate, Comment);


              } else {
                  //test.setText("Network Failed");
                  Context context = getApplicationContext();
                  CharSequence text = "Connection error";
                  int duration = Toast.LENGTH_SHORT;

                  Toast toast = Toast.makeText(context, text, duration);
                  toast.show();

              }
          }
        else {
            rateroom.setError("Rate from 1-4");
            rateroom.requestFocus();
        }
        }

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }




}