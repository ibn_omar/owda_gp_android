package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by fox on 18/06/17.
 */

public class LoadCountries  extends AsyncTask<String,Void,String> {
    private Context context;
    ProgressDialog loading;
    public String reus;
    String Country_id []=new String[242];
    String Country_name []=new String[242];
    private TextView textViewJSON;
    String Themail;
    JSONObject jsonobject;
    JSONArray jsonarray;
    String load;
    String pic;
    ProgressDialog mProgressDialog;
    ArrayList<String> worldlist;
    String test;
    String user;
    public LoadCountries(Context context) {
        this.context = context;

    }
    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }


    @Override
    protected String doInBackground(String... arg0) {
         load = arg0[0];
        test = arg0[1];
        user = arg0[2];

        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {

            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            //String parameters = "email="+email+"&password="+password;

            url = new URL("http://owda.esy.es/owda/backend/user/get-all-countries");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");


            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;


        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }

    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
//
        //this.textViewJSON.setText(result);

        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                //worldlist = new ArrayList<String>();

                jsonarray = jsonObj.getJSONArray("Countries");
                for (int i = 0; i < jsonarray.length(); i++) {
                    jsonObj = jsonarray.getJSONObject(i);
                    String id = jsonObj.getString("id");
                    String name = jsonObj.getString("name");
                    Country_id[i]=id;
                    Country_name[i]=name;


                }
                if(test=="hi0") {
                    Toast.makeText(context, "Successfull.", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context, sign_up1.class);
                    //myIntent.putExtra("Country", Country);
                    myIntent.putExtra("id", Country_id);
                    myIntent.putExtra("name", Country_name);
                    context.startActivity(myIntent);
                }

                else if(test=="hi1"){
                    Intent myIntent1 = new Intent(context, SearchINAnotherCountry.class);
                    //myIntent.putExtra("Country", Country);
                    myIntent1.putExtra("Country_id", Country_id);
                    myIntent1.putExtra("user_id", load);
                    //myIntent.putExtra("pic", pic);
                    myIntent1.putExtra("Country_name", Country_name);
                    context.startActivity(myIntent1);
                }
                else if(test=="hi2") {

                    Toast.makeText(context, "Successfull.", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context, EditProfile.class);
                    //myIntent.putExtra("Country", Country);
                    myIntent.putExtra("Country_id", Country_id);
                    myIntent.putExtra("id", load);
                    myIntent.putExtra("user", user);
                    //myIntent.putExtra("pic", pic);
                    myIntent.putExtra("Country_name", Country_name);
                    context.startActivity(myIntent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }

}

