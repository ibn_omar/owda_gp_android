package com.example.ibnomar.owda;

import android.content.Intent;
import android.graphics.Color;
import android.provider.ContactsContract;
import android.os.AsyncTask;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.TextView;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONStringer;
import org.json.JSONTokener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import android.text.style.ClickableSpan;


public class signin extends AppCompatActivity {

    protected EditText email;
    private EditText password;
    TextView test2;
     Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        //test2 =(TextView) findViewById(R.id.test2);

        SpannableString ss = new SpannableString("by sign in you agree to our terms and privacy policy");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                startActivity(new Intent(signin.this, Terms.class));
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);

            }

        };
        ss.setSpan(clickableSpan,27,52, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        TextView textView = (TextView) findViewById(R.id.terms);
        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.RED);


        SpannableString ss1 = new SpannableString("Forgot Password ?");
        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                startActivity(new Intent(signin.this, ResetPassword.class));
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);

            }

        };
        ss1.setSpan(clickableSpan1,0,15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        TextView textView2 = (TextView) findViewById(R.id.resetPassword);
        textView2.setText(ss1);
        textView2.setMovementMethod(LinkMovementMethod.getInstance());
        textView2.setHighlightColor(Color.RED);

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);



        Button loginButton = (Button) findViewById(R.id.login);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String E = password.getText().toString();
                String P = email.getText().toString();
                /*if (email.getText().toString().length() == 0) {
                    email.setError("Email is Required");
                    email.requestFocus();
                }
                if (password.getText().toString().length() == 0) {
                    password.setError("Password not entered");
                    password.requestFocus();
                }*/


                Loginfunction(v,E,P);
            }
        });


    }

    public void Loginfunction(View v ,String Email,String Password) {

/*///as we have errors
        Intent secondActivity  = new Intent(LogIn.this,
                LiveStream.class);
        secondActivity.putExtra("Email", "a@yahoo.com");
        startActivity(secondActivity);

////
*/
        Boolean t = isNetworkConnected();
        if (!Email.equals("") && !Password.equals(""))
        {
           if (t == true) {

               new LogInClass(this, test2).execute(Email, Password);
           }
            else {
                //test2.setText("Connection error");
              // Toast.makeText(context, "Connection error", Toast.LENGTH_SHORT).show();
               Context context = getApplicationContext();
               CharSequence text = "Connection error";
               int duration = Toast.LENGTH_SHORT;

               Toast toast = Toast.makeText(context, text, duration);
               toast.show();


            }


        }
        else
        {
                if (email.getText().toString().length() == 0) {
                    email.setError("Email is Required");
                    email.requestFocus();
                }
                if (password.getText().toString().length() == 0) {
                    password.setError("Password not entered");
                    password.requestFocus();
                }
           /* Context context = getApplicationContext();
            CharSequence text ="Fill Email and Password";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();*/

        }
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public void Country(View v){
        Boolean t = isNetworkConnected();
        if (t == true){
            new LoadCountries(this).execute("0");
            //Intent i = new Intent(getApplicationContext(),sign_up1.class);
            //startActivity(i);
        }
        else{
            Context context = getApplicationContext();
            CharSequence text = "Connection error";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }


    @Override
    public void onBackPressed() {
        /*Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);*/
        Intent i = new Intent(getApplicationContext(),splashScreen.class);
        startActivity(i);
        //finish();
    }



}






