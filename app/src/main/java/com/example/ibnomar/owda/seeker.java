package com.example.ibnomar.owda;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class seeker extends AppCompatActivity {
    Context context;
    TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seeker);
        text =(TextView) findViewById(R.id.text);
        finish();
        Intent new_advr = new Intent(this,NavigationBar.class);
        Bundle extras = getIntent().getExtras();
        String id = extras.getString("id");
        new_advr.putExtra("user_id", id);
        startActivity(new_advr);
        Button searchCountry = (Button) findViewById(R.id.searchCountry);

        searchCountry.setOnClickListener(new View.OnClickListener() {

            Boolean t = isNetworkConnected();

            @Override
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                String id = extras.getString("id");
                loadCountry(v,id);



            }

        });

        Button message = (Button) findViewById(R.id.seekinbox);

        message.setOnClickListener(new View.OnClickListener() {

            Boolean t = isNetworkConnected();

            @Override
            public void onClick(View v) {
                if (t == true) {
                    Intent i = new Intent(getApplicationContext(), Messages.class);
                    Bundle extras = getIntent().getExtras();
                    String id = extras.getString("id");
                    i.putExtra("user_id", id);
                    startActivity(i);
                }
                else {
                    //text.setText("Connection error");
                    //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
                    Context context = getApplicationContext();
                    CharSequence text = "Connection error";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                }


            }

        });

        Button searchByPriceCity = (Button) findViewById(R.id.searchbycityprice);

        searchByPriceCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Bundle extras = getIntent().getExtras();
                String id = extras.getString("id");
                LoadCity(v,id);
            }
        });

        Button button1 = (Button) findViewById(R.id.seekprofile);

        button1.setOnClickListener(new View.OnClickListener(){
            Boolean t = isNetworkConnected();

            @Override
            public void onClick(View v) {
                if (t == true) {
                    Intent i = new Intent(getApplicationContext(), MyProfile.class);
                    Bundle extras = getIntent().getExtras();
                    String id = extras.getString("id");
                    i.putExtra("id", id);
                    startActivity(i);
                }
                else {
                    //text.setText("Connection error");
                    //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
                    Context context = getApplicationContext();
                    CharSequence text = "Connection error";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                } }});
        Button logout = (Button) findViewById(R.id.seeklogout);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                logout(v);
            }
        });

        Button search = (Button) findViewById(R.id.seeksearch);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle extras = getIntent().getExtras();
                String user_id = extras.getString("id");
                AllAds(v,user_id);
            }
        });
        Button button2 = (Button) findViewById(R.id.seeksharedroom);


        button2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
                Bundle extras = getIntent().getExtras();
                String user_id = extras.getString("id");

                SeekerRooms(v,user_id);


            }   });
        Button button3 = (Button) findViewById(R.id.seekmeats);


        button3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
                Bundle extras = getIntent().getExtras();
                String user_id = extras.getString("id");
                AllMates(v,user_id);

            }   });



    }
    public void logout(View v ) {
        Boolean t = isNetworkConnected();

        if (t == true) {

            new Logout(this).execute();
        }
        else {
            //text.setText("Connection error");
            //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
            Context context = getApplicationContext();
            CharSequence text = "Connection error";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }

    }


    public void AllAds(View v ,String user_id) {
        Boolean t = isNetworkConnected();

        if (t == true) {

            new AllAds(this).execute(user_id);
        }
        else {
            //text.setText("Connection error");
            //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
            Context context = getApplicationContext();
            CharSequence text = "Connection error";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }

    }
    public void LoadCity(View v ,String id) {
        Boolean t = isNetworkConnected();

        if (t == true) {
            new LoadCities(this).execute(id);
        }
        else {
            //text.setText("Connection error");
            //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
            Context context = getApplicationContext();
            CharSequence text = "Connection error";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }

    }
    public void SeekerRooms(View v,String user_id) {
        Boolean t = isNetworkConnected();

        if (t == true) {


            new SeekerRooms(this).execute(user_id);
        } else {
            //text.setText("Connection error");
            //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
            Context context = getApplicationContext();
            CharSequence text = "Connection error";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }
    }
    public void AllMates(View v ,String user_id ) {
        Boolean t = isNetworkConnected();

        if (t == true) {

            new SeekerMates(this).execute(user_id);
        }
        else {
            //text.setText("Connection error");
            //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
            Context context = getApplicationContext();
            CharSequence text = "Connection error";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }

    }
    public void loadCountry(View v ,String user_id ) {
        Boolean t = isNetworkConnected();

        if (t == true) {

            new LoadCountries(this).execute(user_id,"hi1");
        }
        else {
            Context context = getApplicationContext();
            CharSequence text = "Connection error";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }

    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onBackPressed() {
        /*Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);*/
        Intent i = new Intent(getApplicationContext(),user.class);
        Bundle extras = getIntent().getExtras();
        String id = extras.getString("id");
        i.putExtra("id", id);
        startActivity(i);
        //finish();
    }
}
