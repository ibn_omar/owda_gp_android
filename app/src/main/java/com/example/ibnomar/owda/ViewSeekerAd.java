package com.example.ibnomar.owda;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ViewSeekerAd extends AppCompatActivity {
    TextView showRate;
    TextView Title;
    TextView body;
    TextView price;
    TextView Reseved;
    TextView idInAdvancepay;
    TextView maintain;
    TextView beds;
    TextView availableBed;
    TextView usedas;
    TextView hasbath;
    TextView hasbalacony;
    TextView fromDate;
    TextView toDate;
    TextView ExpireDate;
    TextView addid;
    Context context;
    String Adv_id ;
    String room_id ;
    String user_id ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_seeker_ad);
        Title = (TextView) findViewById(R.id.Title);
        body = (TextView) findViewById(R.id.body);
        price = (TextView) findViewById(R.id.price);
        beds = (TextView) findViewById(R.id.beds);
        //text5 = (TextView) findViewById(R.id.text5);
        availableBed = (TextView) findViewById(R.id.availableBed);
        maintain = (TextView) findViewById(R.id.maintain);
        ExpireDate = (TextView) findViewById(R.id.ExpireDate);
        Reseved = (TextView) findViewById(R.id.Reseved);
        fromDate = (TextView) findViewById(R.id.fromDate);
        toDate = (TextView) findViewById(R.id.toDate);
        usedas = (TextView) findViewById(R.id.usedas);
        idInAdvancepay = (TextView) findViewById(R.id.idInAdvancepay);
        hasbath = (TextView) findViewById(R.id.hasbath);
        hasbalacony = (TextView) findViewById(R.id.hasbalacony);
        showRate = (TextView) findViewById(R.id.showRate);
        addid = (TextView) findViewById(R.id.addid);
        Bundle extras = getIntent().getExtras();
        Adv_id = extras.getString("Adv_id");
        room_id = extras.getString("room_id");
        user_id = extras.getString("user_id");
        //final String adv_id = extras.getString("adv_id");
        /*text2.setText(adv_id);
        Toast.makeText(context, adv_id, Toast.LENGTH_SHORT).show();*/


        new ViewAdData(this,Reseved,showRate,Title,body,price,maintain,idInAdvancepay,beds,availableBed,usedas,hasbath,hasbalacony,fromDate,toDate,ExpireDate,addid).execute(Adv_id);

        Button button = (Button) findViewById(R.id.Reserve);
        button.setOnClickListener(new View.OnClickListener() {

            Boolean t = isNetworkConnected();

            @Override
            public void onClick(View v) {

                    /*Intent i = new Intent(getApplicationContext(), user.class);
                    //Bundle extras = getIntent().getExtras();
                    i.putExtra("Adv_id", Adv_id);
                    i.putExtra("room_id", room_id);
                    i.putExtra("user_id", user_id);
                    startActivity(i);*/
               Reserve(v,user_id,room_id);


            }

        });
    }

    public void Reserve(View v ,String user_id ,String room_id) {
        Boolean t = isNetworkConnected();

        if (t == true) {

            new PayAndReserve(this).execute(user_id,room_id);
        }
        else {
            //text.setText("Connection error");
            //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
            Context context = getApplicationContext();
            CharSequence text = "Connection error";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }

    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}

