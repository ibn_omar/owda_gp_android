package com.example.ibnomar.owda;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SendMessage extends AppCompatActivity {
    private EditText userName;
    private EditText subject;
    private EditText body;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);


        userName = (EditText) findViewById(R.id.userName);
        subject = (EditText) findViewById(R.id.subject);
        body = (EditText) findViewById(R.id.body);

        Bundle extras = getIntent().getExtras();
        String from_user = extras.getString("from_user");
        userName.setText(from_user);
        Button send = (Button) findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userNam = userName.getText().toString();
                String subjec = subject.getText().toString();
                String bod = body.getText().toString();



                send(v,userNam,subjec,bod);
            }
        });


    }
    public void send(View v ,String userNamee,String subjectt ,String bodyy) {

        Boolean t = isNetworkConnected();
        if (!userNamee.equals("") && !subjectt.equals("")&& !bodyy.equals(""))
        {
            if (t == true) {
                Bundle extras = getIntent().getExtras();
                String user_id = extras.getString("user_id");
                String user = extras.getString("user");

                new SubmitSendMessage(this).execute(user_id,userNamee, subjectt,bodyy,user);
            }
            else {
                //test2.setText("Connection error");
                // Toast.makeText(context, "Connection error", Toast.LENGTH_SHORT).show();
                Context context = getApplicationContext();
                CharSequence text = "Connection error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();


            }


        }
        else
        {
            if (userName.getText().toString().length() == 0) {
                userName.setError("UserName is Required");
                userName.requestFocus();
            }
            if (subject.getText().toString().length() == 0) {
                subject.setError("Subject not entered");
                subject.requestFocus();
            }
            if (body.getText().toString().length() == 0) {
                body.setError("Content not entered");
                body.requestFocus();
            }


        }
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    }

