package com.example.ibnomar.owda;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONObject;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class MyProfile extends AppCompatActivity {
    TextView text1;
    TextView text2;
    TextView text3;
    TextView text4;
    //TextView text5;
    TextView text6;
    TextView text7;
    TextView text8;
    TextView text9;
    TextView text10;
    TextView text11;
    TextView text12;
    TextView text13;
    TextView text14;

    Context context;


    private ImageView imageView;

    private Bitmap bitmap;
    String id;
    String user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Boolean t = isNetworkConnected();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        text1 = (TextView) findViewById(R.id.text1);
        text2 = (TextView) findViewById(R.id.text2);
        text3 = (TextView) findViewById(R.id.text3);
        text4 = (TextView) findViewById(R.id.text4);
        //text5 = (TextView) findViewById(R.id.text5);
        text6 = (TextView) findViewById(R.id.text6);
        text7 = (TextView) findViewById(R.id.text7);
        text8 = (TextView) findViewById(R.id.text8);
        text9 = (TextView) findViewById(R.id.text9);
        text10 = (TextView) findViewById(R.id.text10);
        text11 = (TextView) findViewById(R.id.text11);
        text12 = (TextView) findViewById(R.id.text12);
        text13 = (TextView) findViewById(R.id.text13);
        text14 = (TextView) findViewById(R.id.text14);

        Bundle extras = getIntent().getExtras();
        id = extras.getString("id");
        user = extras.getString("user");


        imageView = (ImageView) findViewById(R.id.imageView);

        new ViewProfile(this, text1, text2, text3, text4, text6, text7, text8, text9, text10, text11, text12, text13, text14 ,imageView).execute(id);




        Button button2 = (Button) findViewById(R.id.EditProfile1);

        button2.setOnClickListener(new View.OnClickListener() {

            Boolean t = isNetworkConnected();

            @Override
            public void onClick(View v) {
                Country(v);

                /*if (t == true) {
                    new LoadCountries(this).execute("0");
                    Intent i = new Intent(getApplicationContext(), EditProfile.class);
                    Bundle extras = getIntent().getExtras();
                    String id = extras.getString("id");
                    i.putExtra("id", id);
                    startActivity(i);
                } else {
                    //text.setText("Connection error");
                    //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
                    Context context = getApplicationContext();
                    CharSequence text = "Connection error";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                }*/


            }

        });
    }



    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }



    public void Country(View v) {
        Boolean t = isNetworkConnected();
        if (t == true) {
            Bundle extras = getIntent().getExtras();
            String id = extras.getString("id");
            new LoadCountries(this).execute(id,"hi2",user);
            /*Intent i = new Intent(getApplicationContext(), EditProfile.class);
            Bundle extras = getIntent().getExtras();
            String id = extras.getString("id");
            i.putExtra("id", id);
            startActivity(i);*/
        } else {
            Context context = getApplicationContext();
            CharSequence text = "Connection error";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }
    @Override
    public void onBackPressed() {
        /*Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);*/
        if (user.equals("1")) {
            Intent i = new Intent(getApplicationContext(), advertiser.class);
            Bundle extras = getIntent().getExtras();
            String id = extras.getString("id");
            i.putExtra("id", id);
            startActivity(i);
        }
        else if (user.equals("0")){
            Intent i = new Intent(getApplicationContext(), seeker.class);
            Bundle extras = getIntent().getExtras();
            String id = extras.getString("id");
            i.putExtra("id", id);
            startActivity(i);

        }
        //finish();
    }
}