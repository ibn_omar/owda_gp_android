package com.example.ibnomar.owda;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Intent;
import android.widget.Toast;

public class UserRate extends AppCompatActivity {

    TextView first;
    TextView last;
    TextView usrate;
    TextView Email;
    TextView user;
    TextView face;
    TextView phonenum;
    TextView nationality;
    TextView age;
    TextView edu;
    TextView gender;
    TextView mstatus;
    TextView smoker;
    TextView test;
    EditText rateM;
    EditText commentM;
    Button submitRate;

    Context context;

    String mate_id;
    String user_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_rate);
        submitRate = (Button) findViewById(R.id.submitRateMate);
        rateM = (EditText) findViewById(R.id.RateM);
        commentM = (EditText) findViewById(R.id.CommentM);
        first = (TextView) findViewById(R.id.FirstName);
        last = (TextView) findViewById(R.id.LastName);
        usrate = (TextView) findViewById(R.id.Rate);
        user=(TextView) findViewById(R.id.User);
        Email = (TextView) findViewById(R.id.Email);
        //text5 = (TextView) findViewById(R.id.text5);
        face = (TextView) findViewById(R.id.Face);
        phonenum = (TextView) findViewById(R.id.Phone);
        nationality = (TextView) findViewById(R.id.Nationality);
        age = (TextView) findViewById(R.id.Age);
        edu = (TextView) findViewById(R.id.Edu);
        gender = (TextView) findViewById(R.id.Gender);
        mstatus = (TextView) findViewById(R.id.Mstatus);
        smoker = (TextView) findViewById(R.id.Smoke);
        //text14 = (TextView) findViewById(R.id.text14);
        Bundle extras = getIntent().getExtras();
        user_id = extras.getString("user_id");
        mate_id = extras.getString("mate_id");
        new ViewRateMateClass(this, usrate,first, last,user, Email,  face, phonenum, nationality, age, edu, gender, mstatus, smoker).execute(mate_id);
        submitRate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {





                String mr = rateM.getText().toString();
                String mc = commentM.getText().toString();
                String mid = mate_id;
                String usrid = user_id ;

                MRate(v,usrid,mid  ,mr,mc );
            }
        });
    }

    public void MRate(View v,String userid ,   String mateid , String rate,String Comment ) {

        Boolean t = isNetworkConnected();
        if (!rate.equals("") && !Comment.equals("") ) {
            if(rate.equals("1") || rate.equals("2") || rate.equals("3") || rate.equals("4")) {
                if (t == true) {

                    new UserRateClass(this, test).execute(userid, mateid, rate, Comment);


                } else {
                    //test.setText("Network Failed");
                    Context context = getApplicationContext();
                    CharSequence text = "Connection error";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                }
            }
            else {
                rateM.setError("Rate from 1-4");
                rateM.requestFocus();
            }
        }

    }




    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
