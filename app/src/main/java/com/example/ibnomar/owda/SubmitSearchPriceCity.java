package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by fox on 23/06/17.
 */

public class SubmitSearchPriceCity   extends AsyncTask<String,Void,String> {
    private Context context;
    ProgressDialog loading;
    String theMail;
    String user_id;
    JSONObject jsonobject;
    JSONArray ads;
    JSONArray advertisements;
    public String reus;
    private TextView textViewJSON;

    public SubmitSearchPriceCity(Context context) {

        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }



    @Override
    protected String doInBackground(String... arg0) {
        user_id= arg0[0];
        String city_id = arg0[1];
        String pricee = arg0[2];

        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "price=" + URLEncoder.encode(pricee, "UTF-8");
            data += "&city_id=" + URLEncoder.encode(city_id, "UTF-8");
            data += "&user_id=" + URLEncoder.encode(user_id, "UTF-8");



            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            url = new URL("http://owda.esy.es/owda/backend/advertisement/get-filtered-ads");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();
            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;

        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;

        //this.textViewJSON.setText(result);
        if (jsonStr != null) {
            try {
               JSONObject jsonObj = new JSONObject(jsonStr);
                ads = jsonObj.getJSONArray("ads");

                JSONObject advertisement ;
               // advertisements = jsonObj.getJSONObject("advertisements");
                String room_id []=new String[ads.length()];
                String ads_title []=new String[ads.length()];
                String ads_price []=new String[ads.length()];
                String ads_id []=new String[ads.length()];
                for (int i = 0; i < ads.length(); i++) {
                    JSONObject temp ;
                    temp=ads.getJSONObject(i);

                    advertisements = temp.getJSONArray("advertisements");
                    advertisement = advertisements.getJSONObject(0);
                    String title = advertisement.getString("title");
                    String price = advertisement.getString("price");
                    String id = advertisement.getString("id");
                    String room_idd=advertisement.getString("room_id");
                    ads_title[i] = title;
                    ads_price[i] = price;
                    ads_id[i] = id;
                    room_id[i]= room_idd;
                }


if(ads_id.length!=0) {

    Toast.makeText(context, "Ads in City.", Toast.LENGTH_SHORT).show();
    Intent myIntent = new Intent(context, AdvListView.class);
    //myIntent.putExtra("Country", Country);
    myIntent.putExtra("ads_title", ads_title);
    myIntent.putExtra("ads_price", ads_price);
    myIntent.putExtra("ads_id", ads_id);
    myIntent.putExtra("room_id", room_id);
    myIntent.putExtra("user_id", user_id);
    context.startActivity(myIntent);
}
else if(ads_id.length==0){
    Toast.makeText(context, "No Ads in this City.", Toast.LENGTH_SHORT).show();

}
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }

    }
}
