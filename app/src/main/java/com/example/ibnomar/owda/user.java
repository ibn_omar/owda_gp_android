package com.example.ibnomar.owda;

import android.view.View.OnClickListener;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class user extends AppCompatActivity {

TextView text1;
    TextView text2;
    TextView text3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        /*Bundle extras = getIntent().getExtras();
        String Adv_id = extras.getString("Adv_id");
        String room_id = extras.getString("room_id");
        String user_id = extras.getString("user_id");
        text1 = (TextView) findViewById(R.id.tes1);
        text1.setText(Adv_id);
        text2 = (TextView) findViewById(R.id.tes2);
        text2.setText(room_id);
        text3 = (TextView) findViewById(R.id.tes3);
        text3.setText(user_id);*/
        Button button1 = (Button) findViewById(R.id.advertiser);
        Button button2 = (Button) findViewById(R.id.seeker);


        button1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0)
            {   Intent i = new Intent(getApplicationContext(),advertiser.class);
                Bundle extras = getIntent().getExtras();
                String id = extras.getString("id");
                i.putExtra("id", id);
                startActivity(i);

            }   });

        button2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0)
            {   Intent i = new Intent(getApplicationContext(),seeker.class);
                Bundle extras = getIntent().getExtras();
                String id = extras.getString("id");
                i.putExtra("id", id);
                startActivity(i);

            }   });
    }

    @Override
    public void onBackPressed() {
        /*Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);*/
        Intent i = new Intent(getApplicationContext(),user.class);
        Bundle extras = getIntent().getExtras();
        String id = extras.getString("id");
        i.putExtra("id", id);
        startActivity(i);
        //finish();
    }

}

