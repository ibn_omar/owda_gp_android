package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by fox on 29/06/17.
 */

public class GetMyMessage extends AsyncTask<String,Void,String> {
    private Context context;
    ProgressDialog loading;
    public String reus;
    private TextView textViewJSON;
    String Themail;
    JSONObject jsonobject;
    JSONArray jsonarray;
    String load;
    ProgressDialog mProgressDialog;
    ArrayList<String> worldlist;
    String user_id;
    String user;
    public GetMyMessage(Context context) {
        this.context = context;

    }
    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }


    @Override
    protected String doInBackground(String... arg0) {
        user_id = arg0[0];
        user=arg0[1];
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "user_id=" + URLEncoder.encode(user_id, "UTF-8");

            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            //String parameters = "email="+email+"&password="+password;

            url = new URL("http://owda.esy.es/owda/backend/message/my-messages");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");
            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();

            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;


        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }

    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
//
        //this.textViewJSON.setText(result);

        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                jsonarray = jsonObj.getJSONArray("messages");
                String message_id []=new String[jsonarray.length()];
                String from_user []=new String[jsonarray.length()];
                String subject []=new String[jsonarray.length()];
                for (int i = 0; i < jsonarray.length(); i++) {
                    jsonObj = jsonarray.getJSONObject(i);
                    String id = jsonObj.getString("id");
                    String fromuser = jsonObj.getString("from_user");
                    String subj = jsonObj.getString("subject");
                    message_id[i]=id;
                    from_user[i]=fromuser;
                    subject[i]=subj;


                }

                Toast.makeText(context, "Load Messages successfull.", Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(context, MyMessagesListView.class);
                myIntent.putExtra("message_id", message_id);
                myIntent.putExtra("from_user", from_user);
                myIntent.putExtra("subject", subject);
                myIntent.putExtra("user_id", user_id);
                myIntent.putExtra("user", user);
                context.startActivity(myIntent);

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }

}
