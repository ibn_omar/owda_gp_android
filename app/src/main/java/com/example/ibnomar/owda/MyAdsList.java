package com.example.ibnomar.owda;

import android.support.v7.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.app.Activity;
import android.os.Bundle;
import android.widget.*;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.view.LayoutInflater;
import android.app.ListActivity;
import android.os.Bundle;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import android.content.ContentUris;
import android.net.Uri;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.os.Bundle;
import android.widget.Toast;
import android.os.Bundle;

public class MyAdsList extends AppCompatActivity {
    String ads_id [];
    String ads_title [];
    String ads_price [];
    String Adv_id;
    Button button;
    String user_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ads_list);
        ListView ListView = (ListView)findViewById(R.id.MyAdsList);
        Bundle extras = getIntent().getExtras();
        ads_id = extras.getStringArray("ads_id");
        ads_title = extras.getStringArray("ads_title");
        ads_price = extras.getStringArray("ads_price");
        user_id = extras.getString("user_id");
        MyAdsList.CustomAdapter customAdapter = new MyAdsList.CustomAdapter();
        ListView.setAdapter(customAdapter);
        ListView.setClickable(true);
        ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Intent i = new Intent(getApplicationContext(),viewAdvertisement.class);
                i.putExtra("Adv_id",ads_id[position]);
                i.putExtra("user_id",user_id);
                startActivity(i);
            }
        });

    }
    class CustomAdapter extends BaseAdapter{


        @Override
        public int getCount() {
            return ads_id.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.custommyadslist,null);

            TextView textView_name1 = (TextView)view.findViewById(R.id.texttiltle);
            TextView textView_name2 = (TextView)view.findViewById(R.id.textprice);

            textView_name1.setText(ads_title[i]);
            textView_name2.setText(ads_price[i]);
            //Adv_id=ads_id[i];
            //button.setTag(textView_name1.getText().toString());


            return view;

        }

    }
}
