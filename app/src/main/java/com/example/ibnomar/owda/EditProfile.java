package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class EditProfile extends AppCompatActivity {
    EditText edtUsername;
    EditText edtEmail;
    EditText edtPass;
    EditText edtConfirmPass;
    EditText edtFirst;
    EditText edtLast;
    EditText edtage;
    EditText edtPhone;
    EditText edtFBAccount;
    TextView test;
    RadioGroup radioSex,mStatus,smoke,education;
    RadioButton rSex,rStatus,rSmoke,rEducation;
    Context context;
    String id;
    String user;
    Spinner spin;
    String pic;
    private Button buttonChoose;
    private ImageView imageView;

    private Bitmap bitmap;
    Bitmap scaled;
    private int PICK_IMAGE_REQUEST = 1;
    private Uri filePath;

    /*private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //imageView.setImageBitmap(bitmap);

                //bitmap = BitmapFactory.decodeFile(filePath);
                int nh = (int) ( bitmap.getHeight() * (512.0 / bitmap.getWidth()) );
                 scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                imageView.setImageBitmap(scaled);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        //bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        edtUsername = (EditText) findViewById(R.id.edtUsername);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPass = (EditText) findViewById(R.id.edtPass);
        edtConfirmPass = (EditText) findViewById(R.id.edtConfirmPass);
        test = (TextView) findViewById(R.id.test);
        edtFirst=(EditText)findViewById(R.id.edtFirst);
        edtLast=(EditText)findViewById(R.id.edtLast);
        edtage=(EditText)findViewById(R.id.edtage);
        edtPhone=(EditText)findViewById(R.id.edtPhone);
        edtFBAccount=(EditText)findViewById(R.id.edtFBAccount);

        String Country_id []=new String[242];
        String Country_name []=new String[242];
        Bundle extras = getIntent().getExtras();
        Country_id = extras.getStringArray("Country_id");
        Country_name = extras.getStringArray("Country_name");
        Spinner mySpinner = (Spinner) findViewById(R.id.spinner1);
        mySpinner.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,
                Country_name));

        radioSex=(RadioGroup)findViewById(R.id.radioSex);
        mStatus=(RadioGroup)findViewById(R.id.mStatus);
        smoke=(RadioGroup)findViewById(R.id.smoke);
        education=(RadioGroup)findViewById(R.id.education);
        spin  = (Spinner) findViewById(R.id.spinner1);


       // spin.setOnItemSelectedListener(this);

        //Bundle extras = getIntent().getExtras();
        id = extras.getString("id");
        user = extras.getString("user");

        //pic = extras.getString("pic");

        /*buttonChoose = (Button) findViewById(R.id.buttonChoose);
        imageView = (ImageView) findViewById(R.id.imageView);
        buttonChoose.setOnClickListener(new View.OnClickListener() {

            Boolean t = isNetworkConnected();

            @Override
            public void onClick(View v) {
                showFileChooser();

            }

        });*/







        new ViewInEditProfile(this, edtUsername, edtEmail, edtPass, edtConfirmPass, edtFirst, edtLast, edtage, edtPhone, edtFBAccount, radioSex, mStatus, smoke ,education,spin).execute(id);



        Button Edit = (Button) findViewById(R.id.button1);

        Edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int radioButtonID1 = radioSex.getCheckedRadioButtonId();
                View radioButton1 = radioSex.findViewById(radioButtonID1);
                int gender = radioSex.indexOfChild(radioButton1);

                //int gender =radioSex.getCheckedRadioButtonId();

                int radioButtonID2 = mStatus.getCheckedRadioButtonId();
                View radioButton2 = mStatus.findViewById(radioButtonID2);
                int status = mStatus.indexOfChild(radioButton2);
                //int status =mStatus.getCheckedRadioButtonId();

                int radioButtonID3 = smoke.getCheckedRadioButtonId();
                View radioButton3 = smoke.findViewById(radioButtonID3);
                int Smoke = smoke.indexOfChild(radioButton3);
                //int Smoke =smoke.getCheckedRadioButtonId();


                int radioButtonID4 = education.getCheckedRadioButtonId();
                View radioButton4 = education.findViewById(radioButtonID4);
                int edu = education.indexOfChild(radioButton4)+1;
                //int edu =education.getCheckedRadioButtonId();
                String Sp = spin.getSelectedItem().toString();
                int country;
                //int country = 1;
                if (Sp=="Montenegro"){
                    country=243;
                }
                if (Sp=="Serbia"){
                    country=386;
                }
                else{
                    country=spin.getSelectedItemPosition()+1;
                }

                String usename=edtUsername.getText().toString();
                String Email=edtEmail.getText().toString();
                String Pass=edtPass.getText().toString();
                String ConfPass=edtPass.getText().toString();
                String First=edtFirst.getText().toString();
                String Last=edtLast.getText().toString();
                String Age=edtage.getText().toString();
                String Phone=edtPhone.getText().toString();
                String rate="4";
                //int country=1;
                String Face=edtFBAccount.getText().toString();
               /*if (scaled !=null) {
                   pic = getStringImage(scaled);
               }
               else{*/
                   pic="null";
              //}

                Edit(v,usename,Email,Pass,ConfPass,First,Last,Age,Phone,Face,gender,status,Smoke,edu,country,pic,rate,id);
            }
        });
    }



    public  void Edit(View v,String usename,String Email,String Pass,String ConfPass,String First,String Last,String Age,String Phone,String Face,int gender ,int status,int Smoke, int edu,int country,String pic,String rate,String id) {

        Boolean t = isNetworkConnected();
        Boolean E = isEmailValid(Email);

        if (!Email.equals("") && !usename.equals("")&& !Pass.equals("") &&!ConfPass.equals("") && !Age.equals("") && !First.equals("") && !Last.equals("") && !Phone.equals("") && country!=-1) {
            if (!edtPass.getText().toString().equals(edtConfirmPass.getText().toString())) {
                edtConfirmPass.setError("Password Not matched");
                edtConfirmPass.requestFocus();
            }
            else if (edtPass.getText().toString().length() < 8) {
                edtPass.setError("Password should be atleast of 8 charactors");
                edtPass.requestFocus();
            }
            else if (E!=true) {
                edtEmail.setError("Email should be Contain @ or .  ");
                edtEmail.requestFocus();
            }
            else {
            if (t == true) {
                new SubmitEditProfile(this, test).execute(usename,Email,Pass,ConfPass,First,Last,Age,Phone,Face,String.valueOf(gender),String.valueOf(status),String.valueOf(Smoke),String.valueOf(edu),String.valueOf(country),pic,rate,id,user);

            } else {
                Context context = getApplicationContext();
                CharSequence text = "Connection error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

            }
            }
        } else {

            if (edtUsername.getText().toString().length() == 0) {
                edtUsername.setError("Username is Required");
                edtUsername.requestFocus();
            } else if (edtPass.getText().toString().length() == 0) {
                edtPass.setError("Password not entered");
                edtPass.requestFocus();
            } else if (edtConfirmPass.getText().toString().length() == 0) {
                edtConfirmPass.setError("Please confirm password");
                edtConfirmPass.requestFocus();
            } else if (!edtPass.getText().toString().equals(edtConfirmPass.getText().toString())) {
                edtConfirmPass.setError("Password Not matched");
                edtConfirmPass.requestFocus();
            } else if (edtPass.getText().toString().length() < 8) {
                edtPass.setError("Password should be atleast of 8 charactors");
                edtPass.requestFocus();


            } else if (edtEmail.getText().toString().length() == 0) {
                edtEmail.setError("Email is Required");
                edtEmail.requestFocus();
            } else if (edtFirst.getText().toString().length() == 0) {
                edtFirst.setError("First name not entered");
                edtFirst.requestFocus();
            } else if (edtLast.getText().toString().length() == 0) {
                edtLast.setError("Last name not entered");
                edtLast.requestFocus();
            } else if (edtage.getText().toString().length() == 0) {
                edtage.setError("Age is Required");
                edtage.requestFocus();
            } else if (edtPhone.getText().toString().length() == 0) {
                edtPhone.setError("Phone is Required");
                edtPhone.requestFocus();
            } /*else if (edtFBAccount.getText().toString().length() == 0) {
                edtFBAccount.setError("Facebook Account is Required");
             +   edtFBAccount.requestFocus();
            }*/

        }
    }
    private boolean isNetworkConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    }


