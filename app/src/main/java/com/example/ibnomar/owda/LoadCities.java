package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by fox on 23/06/17.
 */

public class LoadCities  extends AsyncTask<String,Void,String> {
    private Context context;
    ProgressDialog loading;
    public String reus;
    private TextView textViewJSON;
    String Themail;
    JSONObject jsonobject;
    JSONArray jsonarray;
    String user_id;
    ProgressDialog mProgressDialog;
    ArrayList<String> worldlist;
    public LoadCities(Context context) {
        this.context = context;

    }
    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }


    @Override
    protected String doInBackground(String... arg0) {
        user_id = arg0[0];

        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "user_id=" + URLEncoder.encode(user_id, "UTF-8");

            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            //String parameters = "email="+email+"&password="+password;

            url = new URL("http://owda.esy.es/owda/backend/advertisement/get-filtered-ads");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(15000);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");
            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();

            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;


        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }

    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
//
        //this.textViewJSON.setText(result);

        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                jsonarray = jsonObj.getJSONArray("cities");
                String Country_id []=new String[jsonarray.length()];
                String City_name []=new String[jsonarray.length()];
                String City_id []=new String[jsonarray.length()];
                for (int i = 0; i < jsonarray.length(); i++) {
                    jsonObj = jsonarray.getJSONObject(i);
                    String id = jsonObj.getString("id");
                    String country_id = jsonObj.getString("country_id");
                    String name = jsonObj.getString("name");
                    Country_id[i]=country_id;
                    City_name[i]=name;
                    City_id[i]=id;


                }

                    Toast.makeText(context, "Successfull.", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context, SearchByPriceCity.class);
                    myIntent.putExtra("Country_id", Country_id);
                    myIntent.putExtra("City_name", City_name);
                    myIntent.putExtra("City_id", City_id);
                    myIntent.putExtra("user_id", user_id);
                    context.startActivity(myIntent);

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }

}

