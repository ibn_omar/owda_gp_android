package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by islam on 21/06/17.
 */

public class ViewAdData extends AsyncTask<String,Void,String> {
    private TextView textViewJSON1;
    private TextView textViewJSON2;
    private TextView textViewJSON3;
    private TextView textViewJSON4;
    private TextView textViewJSON5;
    private TextView textViewJSON6;
    private TextView textViewJSON7;
    private TextView textViewJSON8;
    private TextView textViewJSON9;
    private TextView textViewJSON10;
    private TextView textViewJSON11;
    private TextView textViewJSON12;
    private TextView textViewJSON13;
    private TextView textViewJSON14;
    private TextView textViewJSON15;
    private TextView textViewJSON16;

    private Context context;
    ProgressDialog loading;
    public String reus;
    private TextView textViewJSON;
    String Themail;
    JSONObject jsonobject;
    JSONArray jsonarray;
    String load;
    ProgressDialog mProgressDialog;
    ArrayList<String> worldlist;
    String user_id1;
    //  MyAdapter3 myAdapter3;



    public  ViewAdData(Context context, TextView Reserved ,TextView Rate,TextView title,TextView body,TextView price,TextView maintain,TextView idInAdvancepay,TextView beds,TextView availableBed,TextView usedas,TextView hasbath,TextView hasbalacony,TextView fromDate,TextView toDate,TextView ExpireDate,TextView user) {
        this.context = context;
        this.textViewJSON1 = Reserved;
        this.textViewJSON2 = Rate;
        this.textViewJSON3 = title;
        this.textViewJSON4 = body;
        this.textViewJSON5 = price;
        this.textViewJSON6 = maintain;
        this.textViewJSON7 = idInAdvancepay;
        this.textViewJSON8 = beds;
        this.textViewJSON9 = availableBed;
        this.textViewJSON10 = usedas;
        this.textViewJSON11 = hasbath;
        this.textViewJSON12 = hasbalacony;
        this.textViewJSON13 = fromDate;
        this.textViewJSON14 = toDate;
        this.textViewJSON15 = ExpireDate;
        this.textViewJSON16 = user;


        //     myAdapter3=new MyAdapter3(context);

    }

    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }


    @Override
    protected String doInBackground(String... arg0) {
        String id = arg0[0];


        //Themail =id;
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "add_id=" + URLEncoder.encode(id, "UTF-8");

            URL url = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            //String parameters = "email="+email+"&password="+password;

            //url = new URL("http://owda.esy.es/owda/backend/user/get-user-by-id");
            url = new URL("http://owda.esy.es/owda/backend/advertisement/edit-advert");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();

            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;


        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
        //this.textViewJSON1.setText(result);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                JSONObject room ;
                JSONArray jsonarray;
                room = jsonObj.getJSONObject("room");
                String user_id = jsonObj.getString("user_id");
                int reserve = room.optInt("is_reserved");
                String rate = room.optString("rate");
                String title = jsonObj.getString("title");
                String body = jsonObj.getString("body");
                String price = jsonObj.getString("price");
                int inadv = jsonObj.getInt("inadvance_pay");
                int Maintain = jsonObj.getInt("including_maintenance");
                String ExDate = jsonObj.getString("expire_date");
                String From = room.optString("available_from");
                String substrFrom=From.substring(0,10);
                String to = room.optString("available_to");
                String substrTo=From.substring(0,10);
                String numbeds = room.optString("num_of_beds");
                String availbeds = room.optString("available_beds");
                String useage = room.optString("used_as");
                int bath = room.optInt("has_bathroom");
                int balcon = room.optInt("has_balcony");
                //String room_iddd=room.optString("id");
                //String pic=

                //user_id1 = user_id ;

                if(reserve==1){
                    textViewJSON1.setText("No");

                }
                else{
                    textViewJSON1.setText("Yes");
                }
                textViewJSON2.setText(rate);
                textViewJSON3.setText(title);
                textViewJSON4.setText(body);
                textViewJSON5.setText(price);
                if(Maintain==0){
                    textViewJSON6.setText("No");

                }
                else{
                    textViewJSON6.setText("Yes");
                }
                if(inadv==0){
                    textViewJSON7.setText("No");

                }
                else{
                    textViewJSON7.setText("Yes");
                }
                textViewJSON8.setText(numbeds);
                textViewJSON9.setText(availbeds);
                textViewJSON10.setText(useage);
                if(bath==0){
                    textViewJSON11.setText("No");

                }
                else{
                    textViewJSON11.setText("Yes");
                }
                if(balcon==0){
                    textViewJSON12.setText("No");

                }
                else{
                    textViewJSON12.setText("Yes");
                }
                textViewJSON13.setText(substrFrom);
                textViewJSON14.setText(substrTo);
                textViewJSON15.setText(ExDate);
                textViewJSON16.setText(user_id);








            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }
}
