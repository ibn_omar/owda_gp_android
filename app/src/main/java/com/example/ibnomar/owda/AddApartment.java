package com.example.ibnomar.owda;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddApartment extends AppCompatActivity {
    EditText textAdress, textMetro, textfloor, textnumOfRooms, textmaxInhabitantId, currentInhabitants;
    RadioGroup bathroomRadioGroup, kitchenRadioGroup, WifiRadioGroup, furnatureRadioGroup, petsRadioGroup, genderRadioGroup, elevatorRadioGroup, smokingRadioGroup ,ownerRadioGroup;
    Button AddingApartment;
    String user_id;
    TextView test;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_apartment);
        textAdress = (EditText) findViewById(R.id.addressId);
        textMetro = (EditText) findViewById(R.id.nearstMetro);
        textfloor = (EditText) findViewById(R.id.floorId);
        textnumOfRooms = (EditText) findViewById(R.id.numOfRooms);
        textmaxInhabitantId = (EditText) findViewById(R.id.maxInhabitantId);
        currentInhabitants = (EditText) findViewById(R.id.currentInhabitantsId);
        bathroomRadioGroup = (RadioGroup) findViewById(R.id.bathroomRadioGroup);
        kitchenRadioGroup = (RadioGroup) findViewById(R.id.kitchenRadioGroup);
        WifiRadioGroup = (RadioGroup) findViewById(R.id.WifiRadioGroup);
        furnatureRadioGroup = (RadioGroup) findViewById(R.id.furnatureRadioGroup);
        petsRadioGroup= (RadioGroup) findViewById(R.id.petsRadioGroup);
        genderRadioGroup = (RadioGroup) findViewById(R.id.genderRadioGroup);
        elevatorRadioGroup = (RadioGroup) findViewById(R.id.elevatorRadioGroup);
        smokingRadioGroup = (RadioGroup) findViewById(R.id.smokingRadioGroup);
        ownerRadioGroup = (RadioGroup) findViewById(R.id.OwnerShipType);
        final Spinner spin = (Spinner) findViewById(R.id.citySpinner);
        AddingApartment = (Button) findViewById(R.id.AddingApartment);

        Bundle extras = getIntent().getExtras();
        final String [] cities_id ;
        final String [] cities_name  ;
        //Bundle extras = getIntent().getExtras();
         user_id = extras.getString("id");
        cities_id = extras.getStringArray("cities_id");
        cities_name = extras.getStringArray("cities_name");
        Spinner mySpinner = (Spinner) findViewById(R.id.citySpinner);
        mySpinner.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,
                cities_name));

        AddingApartment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int radioButtonID1 = bathroomRadioGroup.getCheckedRadioButtonId();
                View radioButton1 = bathroomRadioGroup.findViewById(radioButtonID1);
                int radio1 = bathroomRadioGroup.indexOfChild(radioButton1);
                if(radio1==1){
                    radio1=0;
                }
                else if(radio1==0){
                    radio1=1;
                }


                int radioButtonID2 = kitchenRadioGroup.getCheckedRadioButtonId();
                View radioButton2 = kitchenRadioGroup.findViewById(radioButtonID2);
                int radio2 = kitchenRadioGroup.indexOfChild(radioButton2);
                if(radio2==1){
                    radio2=0;
                }
                else if(radio2==0){
                    radio2=1;
                }

                int radioButtonID3 = WifiRadioGroup.getCheckedRadioButtonId();
                View radioButton3 = WifiRadioGroup.findViewById(radioButtonID3);
                int radio3 = WifiRadioGroup.indexOfChild(radioButton3);
                if(radio3==1){
                    radio3=0;
                }
                else if(radio3==0){
                    radio3=1;
                }

                int radioButtonID4 = furnatureRadioGroup.getCheckedRadioButtonId();
                View radioButton4 = furnatureRadioGroup.findViewById(radioButtonID4);
                int radio4 = furnatureRadioGroup.indexOfChild(radioButton4);

                int radioButtonID5 = petsRadioGroup.getCheckedRadioButtonId();
                View radioButton5 = petsRadioGroup.findViewById(radioButtonID5);
                int radio5 = petsRadioGroup.indexOfChild(radioButton5);
                if(radio5==1){
                    radio5=0;
                }
                else if(radio5==0){
                    radio5=1;
                }

                int radioButtonID6 = genderRadioGroup.getCheckedRadioButtonId();
                View radioButton6 = genderRadioGroup.findViewById(radioButtonID6);
                int radio6 = genderRadioGroup.indexOfChild(radioButton6);

                int radioButtonID7 = elevatorRadioGroup.getCheckedRadioButtonId();
                View radioButton7 = elevatorRadioGroup.findViewById(radioButtonID7);
                int radio7 = elevatorRadioGroup.indexOfChild(radioButton7);
                if(radio7==1){
                    radio7=0;
                }
                else if(radio7==0){
                    radio7=1;
                }

                int radioButtonID8 = smokingRadioGroup.getCheckedRadioButtonId();
                View radioButton8 = smokingRadioGroup.findViewById(radioButtonID8);
                int radio8 = smokingRadioGroup.indexOfChild(radioButton8);
                if(radio8==1){
                    radio8=0;
                }
                else if(radio8==0){
                    radio8=1;
                }

                int radioButtonID9 = ownerRadioGroup.getCheckedRadioButtonId();
                View radioButton9 = ownerRadioGroup.findViewById(radioButtonID9);
                int radio9 = ownerRadioGroup.indexOfChild(radioButton9);




                String address = textAdress.getText().toString();
                String metro = textMetro.getText().toString();
                String floorNum = textfloor.getText().toString();
                String NumRooms = textnumOfRooms.getText().toString();
                String maxhabit = textmaxInhabitantId.getText().toString();
                String currenthabit = currentInhabitants.getText().toString();

                String Sp = spin.getSelectedItem().toString();
                int Spinindex=spin.getSelectedItemPosition();
                String city_id=cities_id[Spinindex];

                apartmentData(v,user_id ,address, metro,floorNum,NumRooms,maxhabit,currenthabit,radio1,radio2,radio3,radio4,radio5,radio6,radio7,radio8,radio9,city_id);
            }
        });
    }
    public void apartmentData(View v,String user_id, String address, String metro, String floorNum, String NumRooms, String maxhabit, String currenthabit, int radio1, int radio2, int radio3, int radio4, int radio5,int radio6, int radio7,int radio8,int radio9,String  city_id) {

        Boolean t = isNetworkConnected();

        if (!address.equals("") && !metro.equals("") && !floorNum.equals("") && !NumRooms.equals("") && !maxhabit.equals("") && !currenthabit.equals("") && !city_id.equals("")&& radio1 != -1 && radio2 != -1 && radio3 != -1 && radio4 != -1 && radio5 != -1 && radio6 != -1 && radio7 != -1 && radio8 != -1 && radio9 != -1) {

            if (t == true) {
                new ApartmentClass(this).execute(user_id,address, metro,floorNum,NumRooms,maxhabit,currenthabit,String.valueOf(radio1),String.valueOf(radio2),String.valueOf(radio3),String.valueOf(radio4),String.valueOf(radio5),String.valueOf(radio6),String.valueOf(radio7),String.valueOf(radio8),String.valueOf(radio9),city_id);


            } else {
                //test.setText("Network Failed");
                Context context = getApplicationContext();
                CharSequence text = "Connection error";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

            }

        } else {

            Context context = getApplicationContext();
            CharSequence text = "Please Fill all Fields";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();


        }
    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

}
