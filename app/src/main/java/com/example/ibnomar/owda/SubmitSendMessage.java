package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by fox on 29/06/17.
 */

public class SubmitSendMessage   extends AsyncTask<String,Void,String> {
    private Context context;
    ProgressDialog loading;
    String theMail;

    JSONObject jsonobject;
    JSONArray ads;
    JSONArray advertisements;
    public String reus;

    String user;
    private TextView textViewJSON;

    public SubmitSendMessage(Context context) {

        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }



    @Override
    protected String doInBackground(String... arg0) {
        String user_id= arg0[0];
        String userNamee = arg0[1];
        String subjectt = arg0[2];
        String bodyy = arg0[3];
         user = arg0[4];

        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "Message[from_user]=" + URLEncoder.encode(user_id, "UTF-8");
            data += "&Message[subject]=" + URLEncoder.encode(subjectt, "UTF-8");
            data += "&Message[content]=" + URLEncoder.encode(bodyy, "UTF-8");
            data += "&reciever_username=" + URLEncoder.encode(userNamee, "UTF-8");



            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            url = new URL("http://owda.esy.es/owda/backend/message/send-message");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();
            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;

        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;

        //this.textViewJSON.setText(result);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String user_id = jsonObj.getString("user_id");
                String error = jsonObj.getString("error");
                String error_message = jsonObj.getString("error_msg");
                if (error.equals("0")) {
                    Toast.makeText(context, "Message Successfull.", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context, Messages.class);
                    myIntent.putExtra("user_id", user_id);
                    myIntent.putExtra("user", user);
                    context.startActivity(myIntent);

                } else if (error.equals("1")) {
                    Toast.makeText(context,error_message , Toast.LENGTH_SHORT).show();
                    //Toast.makeText(context,Themail , Toast.LENGTH_SHORT).show();

                } else if (error.equals("2")){
                    Toast.makeText(context, error_message, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }

    }
}