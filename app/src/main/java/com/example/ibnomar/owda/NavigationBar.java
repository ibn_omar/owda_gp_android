package com.example.ibnomar.owda;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class NavigationBar extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

     String Uid ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_bar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle extras = getIntent().getExtras();
        Uid= extras.getString("user_id");

        TextView msg1 = (TextView) findViewById(R.id.msg1);
        TextView msg2 = (TextView) findViewById(R.id.msg2);
        TextView msg3 = (TextView) findViewById(R.id.msg3);
        new notifyUser(this, msg1,msg2,msg3).execute(Uid,"1");



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.DrawerSeeker);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationSeeker);
        navigationView.setNavigationItemSelectedListener(this);
    }

   @Override
    public void onBackPressed() {
       // DrawerLayout drawer = (DrawerLayout) findViewById(R.id.DrawerSeeker);
        //if (drawer.isDrawerOpen(GravityCompat.START)) {
            //drawer.closeDrawer(GravityCompat.START);
            Intent i = new Intent(getApplicationContext(), NavigationBar.class);
            i.putExtra("user_id", Uid);
            startActivity(i);
       /* } else {
            super.onBackPressed();
        }*/
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
;
        if (id == R.id.seek_myProfile) {
            Intent i = new Intent(getApplicationContext(), MyProfile.class);
            i.putExtra("id", Uid);
            i.putExtra("user","0");
            startActivity(i);
            return true;

        } else if (id == R.id.seek_myRooms) {
            new SeekerRooms(this).execute(Uid);
            return true;
        } else if (id == R.id.myInbox) {

            Intent i = new Intent(getApplicationContext(), Messages.class);
            i.putExtra("user_id", Uid);
            i.putExtra("user","0");
            startActivity(i);
            return true;
        } else if (id == R.id.nav_my_mates) {
            new SeekerMates(this).execute(Uid);
            return true;

        } else if (id == R.id.nav_all) {

            new AllAds(this).execute(Uid);

            return true;

        } else if (id == R.id.search_nav) {
            new LoadCities(this).execute(Uid);

            return true;

        }
        else if (id == R.id.searchOut) {
            new LoadCountries(this).execute(Uid,"hi1","");

            return true;

        }
        else if (id == R.id.logOut) {
            new Logout(this).execute();

            return true;

        }




        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.DrawerSeeker);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



}
