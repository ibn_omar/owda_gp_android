package com.example.ibnomar.owda;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Messages extends AppCompatActivity {
String user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        Bundle extras = getIntent().getExtras();
        user = extras.getString("user");

        Button send = (Button) findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {

            Boolean t = isNetworkConnected();

            @Override
            public void onClick(View v) {
                if (t == true) {
                    Intent i = new Intent(getApplicationContext(), SendMessage.class);
                    Bundle extras = getIntent().getExtras();
                    String id = extras.getString("user_id");
                    i.putExtra("user_id", id);
                    i.putExtra("user", user);

                    startActivity(i);
                }
                else {
                    //text.setText("Connection error");
                    //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
                    Context context = getApplicationContext();
                    CharSequence text = "Connection error";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                }


            }

        });
        Button myMessage = (Button) findViewById(R.id.mymessage);

        myMessage.setOnClickListener(new View.OnClickListener() {

            Boolean t = isNetworkConnected();

            @Override
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                String user_id = extras.getString("user_id");
                String user = extras.getString("user");
                Mymessage(v,user_id,user);



            }

        });
    }

    public void Mymessage(View v ,String user_id,String user ) {
        Boolean t = isNetworkConnected();

        if (t == true) {

            new GetMyMessage(this).execute(user_id,user);
        }
        else {
            Context context = getApplicationContext();
            CharSequence text = "Connection error";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }

    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    @Override
    public void onBackPressed() {
       /*Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);*/
        Bundle extras = getIntent().getExtras();
        user = extras.getString("user");
        if (user.equals("1")) {
            Intent i = new Intent(getApplicationContext(), advertiser.class);
            String id = extras.getString("user_id");
            i.putExtra("id", id);
            startActivity(i);
        }
        else if (user.equals("0")){
            Intent i = new Intent(getApplicationContext(), seeker.class);
            String id = extras.getString("user_id");
            i.putExtra("id", id);
            startActivity(i);

        }
        //finish();
    }
}
