package com.example.ibnomar.owda;

/**
 * Created by islam on 14/06/17.
 */
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;
import android.os.Bundle;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.io.OutputStreamWriter;


public class AddAvertisementClass extends AsyncTask<String,Void,String>{

    private Context context;
    ProgressDialog loading;
    String theMail;
    public String reus;
    private TextView textViewJSON;
    String user_id;
    public AddAvertisementClass(Context context, TextView textViewJSON) {

        this.context = context;
        this.textViewJSON=textViewJSON;
    }

    @Override
    protected void onPreExecute() {
        loading=  ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }
    @Override
    protected String doInBackground(String... arg0) {
        String num_of_beds = arg0[0];
        String available_beds = arg0[1];
        String used_as=arg0[2];
        String has_bathroom = arg0[3];
        String has_balcony = arg0[4];
        //String rate = arg0[5];
        //String is_reserved = arg0[6];
        String available_from = arg0[5];
        String available_to = arg0[6];

        String title = arg0[7];
        String body = arg0[8];
        String price = arg0[9];
        String inadvance_pay=arg0[10];
        String including_maintenance = arg0[11];
        user_id=arg0[12];
        String picture=arg0[13];
        //String user_id="27";

        //theMail = email;
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            //Toast.makeText(context, "Before URL.", Toast.LENGTH_SHORT).show();
            data = "Room[num_of_beds]=" + URLEncoder.encode(num_of_beds, "UTF-8");
            data += "&Room[available_beds]=" + URLEncoder.encode(available_beds, "UTF-8");
            data += "&Room[used_as]=" + URLEncoder.encode(used_as, "UTF-8");
            data += "&Room[has_bathroom]=" + URLEncoder.encode(has_bathroom, "UTF-8");
            data += "&Room[has_balcony]=" + URLEncoder.encode(has_balcony, "UTF-8");
            //data += "&Room[rate]=" + URLEncoder.encode(rate, "UTF-8");
            data += "&Room[available_from]=" + URLEncoder.encode(available_from, "UTF-8");
            data += "&Room[available_to]=" + URLEncoder.encode(available_to, "UTF-8");
            data += "&Room[picture]=" + URLEncoder.encode(picture, "UTF-8");
            data += "&Advertisement[title]=" + URLEncoder.encode(title, "UTF-8");
            data += "&Advertisement[body]=" + URLEncoder.encode(body, "UTF-8");
            data += "&Advertisement[price]=" + URLEncoder.encode(price, "UTF-8");
            data += "&Advertisement[inadvance_pay]=" + URLEncoder.encode(inadvance_pay, "UTF-8");
            data += "&Advertisement[including_maintenance]=" + URLEncoder.encode(including_maintenance, "UTF-8");
            data += "&user_id=" + URLEncoder.encode(user_id, "UTF-8");



            //   link = "http://10.0.2.2:8080/LoginGP.php" + data;
            //  link = "http://10.0.2.2:8012/LoginGP.php" + data;
            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            url = new URL("http://owda.esy.es/owda/backend/advertisement/add-new-advert");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();
            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;

        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
        /*if(result.equals("successfully"))
        {

            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
        }
        else{

            Toast.makeText(context, "This Email Is already taken", Toast.LENGTH_SHORT).show();
        }

*/
        //this.textViewJSON.setText(result);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
               //String user_id = jsonObj.getString("user_id");
                String error = jsonObj.getString("error");
                //Toast.makeText(context, "response recieved.", Toast.LENGTH_SHORT).show();
                if (error.equals("0")) {
                    Toast.makeText(context, "Advertisement has been inserted successfully.", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context, advertiser.class);
//                    Bundle extras = getIntent().getExtras();
//                    String user_id = extras.getString("id");
                   myIntent.putExtra("id", user_id);
                    context.startActivity(myIntent);
                } else if (error.equals("1")) {
                    Toast.makeText(context, "Data could not be inserted. ", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(context,Themail , Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(context, "Couldn't connect to remote database.", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }

}