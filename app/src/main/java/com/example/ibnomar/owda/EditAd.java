package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by islam on 17/06/17.
 */

public class EditAd extends AsyncTask<String,Void,String> {
    private Context context;
    ProgressDialog loading;
    String theMail;
    public String reus;
    private TextView textViewJSON;
    String user_id;

    public EditAd(Context context, TextView textViewJSON) {

        this.context = context;
        this.textViewJSON = textViewJSON;
    }

    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }



    @Override
    protected String doInBackground(String... arg0) {
        String num_of_beds = arg0[0];
        String available_beds = arg0[1];
        String used_as=arg0[2];
        String has_bathroom = arg0[3];
        String has_balcony = arg0[4];
        //String rate = arg0[5];
        //String is_reserved = arg0[6];
        String available_from = arg0[5];
        String available_to = arg0[6];
        String picture=arg0[7];
        String title = arg0[8];
        String body = arg0[9];
        String price = arg0[10];
        String inadvance_pay=arg0[11];
        String including_maintenance = arg0[12];
        String Adv_id= arg0[13];
        user_id=arg0[14];
        //String pic="null";

        //theMail = email;
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "Room[num_of_beds]=" + URLEncoder.encode(num_of_beds, "UTF-8");
            data += "&Room[available_beds]=" + URLEncoder.encode(available_beds, "UTF-8");
            data += "&Room[used_as]=" + URLEncoder.encode(used_as, "UTF-8");
            data += "&Room[has_bathroom]=" + URLEncoder.encode(has_bathroom, "UTF-8");
            data += "&Room[has_balcony]=" + URLEncoder.encode(has_balcony, "UTF-8");
            //data += "&Room[rate]=" + URLEncoder.encode(rate, "UTF-8");
            data += "&Room[available_from]=" + URLEncoder.encode(available_from, "UTF-8");
            data += "&Room[available_to]=" + URLEncoder.encode(available_to, "UTF-8");
            data += "&Room[picture]=" + URLEncoder.encode(picture, "UTF-8");
            data += "&Advertisement[title]=" + URLEncoder.encode(title, "UTF-8");
            data += "&Advertisement[body]=" + URLEncoder.encode(body, "UTF-8");
            data += "&Advertisement[price]=" + URLEncoder.encode(price, "UTF-8");
            data += "&Advertisement[inadvance_pay]=" + URLEncoder.encode(inadvance_pay, "UTF-8");
            data += "&Advertisement[including_maintenance]=" + URLEncoder.encode(including_maintenance, "UTF-8");
            data += "&add_id=" + URLEncoder.encode(Adv_id, "UTF-8");
            //data += "&User[rate]=" + URLEncoder.encode(rate, "UTF-8");

            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            url = new URL("http://owda.esy.es/owda/backend/advertisement/edit-advert");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();
            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;

        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;

        this.textViewJSON.setText(result);
        if (jsonStr != null) {
            String error_msg = "";
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String error = jsonObj.getString("error");
                String Adv_id = jsonObj.getString("adv_id");
                if (error.equals("0")){
                    //String user_id = jsonObj.getString("user_id");
                    //String room_id = jsonObj.getString("room_id");
                    Toast.makeText(context, "Updated successfully.", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context, advertiser.class);
                    myIntent.putExtra("id", user_id);
                    //myIntent.putExtra("id", user_id);
                    context.startActivity(myIntent);
                }

                else if (error.equals("1")) {
                    String error_message = jsonObj.getString("error_message");
                    error_msg = error_message;
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "error empty", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }

    }
}
