package com.example.ibnomar.owda;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class advertiserNavigation extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
TextView text;

    String Uid ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertiser_navigation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle extras = getIntent().getExtras();
        Uid= extras.getString("user_id");

        TextView msg1 = (TextView) findViewById(R.id.msg11);
        TextView msg2 = (TextView) findViewById(R.id.msg22);
        TextView msg3 = (TextView) findViewById(R.id.msg33);
        new notifyUser(this, msg1,msg2,msg3).execute(Uid,"2");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.DrawerAdvertisement);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.advNavigation);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.DrawerAdvertisement);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
             drawer.closeDrawer(GravityCompat.START);
        } else {

            super.onBackPressed();
        }*/
        Intent i = new Intent(getApplicationContext(), advertiserNavigation.class);
        i.putExtra("user_id", Uid);
        startActivity(i);
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {


        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_myProfile) {
            Intent i = new Intent(getApplicationContext(), MyProfile.class);
            i.putExtra("user","1");
            i.putExtra("id", Uid);
            startActivity(i);
            return true;
        } else if (id == R.id.nav_myAdv) {


            new AdvAds(this).execute(Uid);

            return true;

        }
        else if (id == R.id.nav_logoutt) {

           // logout();
            new Logout(this).execute();

            return true;

        }
        else if (id == R.id.nav_myInbox) {

                Intent i = new Intent(getApplicationContext(), Messages.class);
            i.putExtra("user","1");
            i.putExtra("user_id", Uid);
                startActivity(i);
            return true;
        }

        else if (id == R.id.nav_adverr) {
            //CheckAp(Uid);
            new CheckApartment(this,text).execute(Uid);

            return true;

        }



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.DrawerAdvertisement);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }



}
