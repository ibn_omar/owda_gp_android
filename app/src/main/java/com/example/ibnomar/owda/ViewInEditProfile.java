package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import java.net.URLEncoder;

/**
 * Created by fox on 15/06/17.
 */

public class ViewInEditProfile extends AsyncTask<String,Void,String> {

    private Context context;
    ProgressDialog loading;
    public String reus;
    private EditText textViewJSON1;
    private EditText textViewJSON2;
    private EditText textViewJSON3;
    private EditText textViewJSON4;
    private EditText textViewJSON5;
    private EditText textViewJSON6;
    private EditText textViewJSON7;
    private EditText textViewJSON8;
    private EditText textViewJSON9;
    private RadioGroup textViewJSON10;
    private RadioGroup textViewJSON11;
    private RadioGroup textViewJSON12;
    private RadioGroup textViewJSON13;
    private Spinner textViewJSON14;
    String Themail;
    public ViewInEditProfile(Context context, EditText textViewJSON1 ,EditText textViewJSON2,EditText textViewJSON3,EditText textViewJSON4,EditText textViewJSON5,EditText textViewJSON6,EditText textViewJSON7,EditText textViewJSON8,EditText textViewJSON9,RadioGroup textViewJSON10,RadioGroup textViewJSON11,RadioGroup textViewJSON12,RadioGroup textViewJSON13 ,Spinner textViewJSON14) {
        this.context = context;
        this.textViewJSON1 = textViewJSON1;
        this.textViewJSON2 = textViewJSON2;
        this.textViewJSON3 = textViewJSON3;
        this.textViewJSON4 = textViewJSON4;
        this.textViewJSON5 = textViewJSON5;
        this.textViewJSON6 = textViewJSON6;
        this.textViewJSON7 = textViewJSON7;
        this.textViewJSON8 = textViewJSON8;
        this.textViewJSON9 = textViewJSON9;
        this.textViewJSON10 = textViewJSON10;
        this.textViewJSON11 = textViewJSON11;
        this.textViewJSON12 = textViewJSON12;
        this.textViewJSON13 = textViewJSON13;
        this.textViewJSON14 = textViewJSON14;


    }
    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }


    @Override
    protected String doInBackground(String... arg0) {
        String id = arg0[0];


        //Themail =id;
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "id=" + URLEncoder.encode(id, "UTF-8");

            URL url = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            //String parameters = "email="+email+"&password="+password;

            //url = new URL("http://owda.esy.es/owda/backend/user/get-user-by-id");
            url = new URL("http://owda.esy.es/owda/backend/user/edit-user");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();

            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;


        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
        //this.textViewJSON1.setText(result);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String id = jsonObj.getString("id");
                String picture = jsonObj.getString("picture");
                String fname = jsonObj.getString("fname");
                String lname = jsonObj.getString("lname");
                String username = jsonObj.getString("username");
                String email = jsonObj.getString("email");
                String password = jsonObj.getString("password");
                String fb_account = jsonObj.getString("fb_account");
                String phone = jsonObj.getString("phone");
                int Nationality = jsonObj.getInt("Nationality");
                String educational_level = jsonObj.getString("educational_level");
                //int educational_level= jsonObj.getInt("educational_level");
                String age = jsonObj.getString("age");
                //String gender = jsonObj.getString("gender");
                int gender= jsonObj.getInt("gender");
                String rate = jsonObj.getString("rate");
                //String marital_status = jsonObj.getString("marital_status");
                int marital_status= jsonObj.getInt("marital_status");
                //String is_smoker = jsonObj.getString("is_smoker");
                int is_smoker = jsonObj.getInt("is_smoker");
                textViewJSON1.setText(username);
                textViewJSON2.setText(email);
                textViewJSON3.setText(password);
                textViewJSON4.setText(password);
                textViewJSON5.setText(fname);
                textViewJSON6.setText(lname);
                textViewJSON7.setText(age);
                textViewJSON8.setText(phone);

                if(fb_account=="null"){
                    textViewJSON9.setText("Not Avaliable");

                }
                else{
                    textViewJSON9.setText(fb_account);
                }


                if(gender==0){
                    textViewJSON10.check(R.id.male);

                }
                else  if(gender==1){
                    textViewJSON10.check(R.id.female);
                }


                if(is_smoker==1){
                    textViewJSON12.check(R.id.no);

                }
                else  if(is_smoker==0){
                    textViewJSON12.check(R.id.yes);
                }


                if(marital_status==0){
                    textViewJSON11.check(R.id.single);

                }
                else  if(marital_status==1){
                    textViewJSON11.check(R.id.married);
                }
                else  if(marital_status==2){
                    textViewJSON11.check(R.id.divorced);
                }
                else  if(marital_status==3){
                    textViewJSON11.check(R.id.widow);
                }


                if(educational_level=="1"){
                    textViewJSON13.check(R.id.Student);
                }
                else  if(educational_level=="2"){
                    textViewJSON13.check(R.id.Bachelor);
                }
                else  if(educational_level=="3"){
                    textViewJSON13.check(R.id.Doctor);
                }
                textViewJSON14.setSelection(Nationality-1);

                /*textViewJSON9.setText(educational_level);
                textViewJSON10.setText(age);
                textViewJSON11.setText(gender);
                textViewJSON12.setText(rate);*/
                //textViewJSON13.setText(marital_status);


            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }

}
