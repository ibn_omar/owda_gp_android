package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by islam on 20/06/17.
 */

public class AdvAds  extends AsyncTask<String,Void,String> {
    private Context context;
    ProgressDialog loading;
    public String reus;
    private TextView textViewJSON;
    String Themail;
    JSONObject jsonobject;
    JSONArray jsonarray;
    String load;
    ProgressDialog mProgressDialog;
    ArrayList<String> worldlist;
    String user_id;
    public AdvAds(Context context) {
        this.context = context;

    }
    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }


    @Override
    protected String doInBackground(String... arg0) {
        user_id = arg0[0];
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "user_id=" + URLEncoder.encode(user_id, "UTF-8");

            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            //String parameters = "email="+email+"&password="+password;

            url = new URL("http://owda.esy.es/owda/backend/advertisement/get-user-ads");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");
            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();

            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;


        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }

    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
//
        //this.textViewJSON.setText(result);

        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                jsonarray = jsonObj.getJSONArray("ads");
                String ads_title []=new String[jsonarray.length()];
                String ads_price []=new String[jsonarray.length()];
                String ads_id []=new String[jsonarray.length()];
                for (int i = 0; i < jsonarray.length(); i++) {
                    jsonObj = jsonarray.getJSONObject(i);
                    String title = jsonObj.getString("title");
                    String price = jsonObj.getString("price");
                    String id = jsonObj.getString("id");
                    ads_title[i]=title;
                    ads_price[i]=price;
                    ads_id[i]=id;


                }

                /*String logged = jsonObj.getString("logged");
                if (logged.equals("0")) {*/

                Toast.makeText(context, "Successfull.", Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(context, MyAdsList.class);
                //myIntent.putExtra("Country", Country);
                myIntent.putExtra("ads_title", ads_title);
                myIntent.putExtra("ads_price", ads_price);
                myIntent.putExtra("ads_id", ads_id);
                myIntent.putExtra("user_id", user_id);
                context.startActivity(myIntent);


                /*} else {
                    Toast.makeText(context, "Logout failed.", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(context,Themail , Toast.LENGTH_SHORT).show();

                }*/
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }

}
