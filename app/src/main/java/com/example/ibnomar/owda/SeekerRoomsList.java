package com.example.ibnomar.owda;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class SeekerRoomsList extends AppCompatActivity {

    String room_from [];
    String room_to [];
    String room_id [];
    String addr [];
    String use [];
    String Adv_id;
    String user_id;
   // Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seeker_rooms_list);
        ListView ListView = (ListView)findViewById(R.id.SeekerRoomsList);
        Bundle extras = getIntent().getExtras();
        room_id = extras.getStringArray("room_id");
        room_from = extras.getStringArray("room_from");
        room_to = extras.getStringArray("room_to");
        addr = extras.getStringArray("addr");
        use = extras.getStringArray("use");
        user_id = extras.getString("user_id");
        SeekerRoomsList.CustomAdapter customAdapter = new SeekerRoomsList.CustomAdapter();
        ListView.setAdapter(customAdapter);
        ListView.setClickable(true);
        ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Intent i = new Intent(getApplicationContext(),RoomRate.class);
                i.putExtra("room_id",room_id[position]);
                i.putExtra("user_id",user_id);
                startActivity(i);
            }
        });

    }
    class CustomAdapter extends BaseAdapter {


        @Override
        public int getCount() {
            return room_id.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.customseekerrooms,null);

            TextView textView_name1 = (TextView)view.findViewById(R.id.textfrom);
            TextView textView_name2 = (TextView)view.findViewById(R.id.textto);
            TextView textView_name3 = (TextView)view.findViewById(R.id.textaddress);
            TextView textView_name4 = (TextView)view.findViewById(R.id.textused);
            textView_name1.setText(room_from[i]);
            textView_name2.setText(room_to[i]);
            textView_name3.setText(addr[i]);
            textView_name4.setText(use[i]);
            //Adv_id=ads_id[i];
            //button.setTag(textView_name1.getText().toString());


            return view;

        }

    }
    public void onBackPressed() {
        /*Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);*/
        Intent i = new Intent(getApplicationContext(),seeker.class);
        Bundle extras = getIntent().getExtras();
        String id = extras.getString("user_id");
        i.putExtra("id", id);
        startActivity(i);
        //finish();
    }
}
