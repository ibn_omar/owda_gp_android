package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by fox on 21/06/17.
 */

public class CheckApartment extends AsyncTask<String,Void,String> {

    private Context context;
    ProgressDialog loading;
    public String reus;
    private TextView textViewJSON;
    String Themail;
    public static String gloable_id;
    String load;
    ProgressDialog mProgressDialog;
    ArrayList<String> worldlist;
    JSONArray cities;

    //  MyAdapter3 myAdapter3;



   public CheckApartment(Context context ,TextView textViewJSON) {
        this.context = context;
       this.textViewJSON=textViewJSON;

    }

    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }


    @Override
    protected String doInBackground(String... arg0) {
        String user_id = arg0[0];


        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "user_id=" + URLEncoder.encode(user_id, "UTF-8");



            URL url = null;
            String response = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;

            url = new URL("http://owda.esy.es/owda/backend/apartment/has-apartment");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(15000);
            connection.setDoInput(true);
            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();
            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;


        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
        //this.textViewJSON.setText(result);

        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                JSONObject city;

                cities = jsonObj.getJSONArray("cities");


               if (cities.length()==0) {
                   String id = jsonObj.getString("user_id");
                   String error = jsonObj.getString("has_apartment");
                    Toast.makeText(context, "You have already Apartment.", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context, AddAdvertisemnet.class);
                    myIntent.putExtra("id",id);
                    context.startActivity(myIntent);

                }

              else{
                   String id = jsonObj.getString("user_id");
                   String error = jsonObj.getString("has_apartment");
                //else if (error.equals("0")) {
                   String [] cities_id =new String[cities.length()];
                   String [] cities_name =new String[cities.length()];
                  for (int i = 0; i < cities.length(); i++) {
                      city = cities.getJSONObject(i);

                      String C_id = city.optString("id");
                      String C_name = city.optString("name");
                      cities_id[i] = C_id;
                      cities_name[i] = C_name;
                  }

                    Toast.makeText(context, "You don't have an Apartment Creat your own.", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context, AddApartment.class);
                    myIntent.putExtra("id", id);
                    myIntent.putExtra("cities_id", cities_id);
                    myIntent.putExtra("cities_name", cities_name);
                    context.startActivity(myIntent);

                } /*else {
                    Toast.makeText(context, "Couldn't connect to remote database.", Toast.LENGTH_SHORT).show();
                }*/
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }


}
