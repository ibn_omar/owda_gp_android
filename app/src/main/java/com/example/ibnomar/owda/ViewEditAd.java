package com.example.ibnomar.owda;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by islam on 17/06/17.
 */

public class ViewEditAd extends AsyncTask<String,Void,String> {

    private Context context;
    ProgressDialog loading;
    public String reus;
    private EditText textViewJSON1;
    private EditText textViewJSON2;
    private EditText textViewJSON3;
    private RadioGroup textViewJSON4;
    private RadioGroup textViewJSON5;
    private EditText textViewJSON6;
    private EditText textViewJSON7;
    private EditText textViewJSON8;
    //private EditText textViewJSON9;
    private EditText textViewJSON10;
    private EditText textViewJSON11;
    private RadioGroup textViewJSON12;
    private RadioGroup textViewJSON13;
    private Spinner textViewJSON14;
    String Themail;
    public ViewEditAd(Context context, EditText NumBeds ,EditText AvailableBeds,EditText  RoomUsage,RadioGroup HasBath,RadioGroup HasBalcony,EditText StartDate,EditText EndDate,EditText AdTitle,EditText AdDiscribtion,EditText AdPrice,RadioGroup InAdvancePay,RadioGroup includeMaintenance) {
        this.context = context;
        this.textViewJSON1 = NumBeds;
        this.textViewJSON2 = AvailableBeds;
        this.textViewJSON3 = RoomUsage;
        this.textViewJSON4 = HasBath;
        this.textViewJSON5 = HasBalcony;
        this.textViewJSON6 = StartDate;
        this.textViewJSON7 = EndDate;
        this.textViewJSON8 = AdTitle;
        this.textViewJSON10= AdDiscribtion;
        this.textViewJSON11 = AdPrice;
        this.textViewJSON12 = InAdvancePay;
        this.textViewJSON13 = includeMaintenance;

    }
    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(this.context, "Loading ...", null, true, true);
    }


    @Override
    protected String doInBackground(String... arg0) {
        String adv_id = arg0[0];
        //Toast.makeText(context, "Adv id =" + adv_id, Toast.LENGTH_SHORT).show();


        //Themail =id;
        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "add_id=" + URLEncoder.encode(adv_id , "UTF-8");

            URL url = null;
            HttpURLConnection connection;
            OutputStreamWriter request = null;
            //String parameters = "email="+email+"&password="+password;

            //url = new URL("http://owda.esy.es/owda/backend/user/get-user-by-id");
            url = new URL("http://owda.esy.es/owda/backend/advertisement/edit-advert");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(data);
            request.flush();
            request.close();

            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            result = bufferedReader.readLine();
            return result;


        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String result) {

        if (loading.isShowing()) {
            loading.dismiss();
        }

        String jsonStr = result;
        this.textViewJSON1.setText(result);
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                JSONObject room ;
                JSONArray jsonarray;
                room = jsonObj.getJSONObject("room");
                //room = jsonarray.getJSONObject(0);
                //String picture = room.optString("picture");
                String used_as = room.optString("used_as");
                String id = jsonObj.getString("id");
                String Title1 = jsonObj.getString("title");
                String body1 = jsonObj.getString("body");
                String price1 = jsonObj.getString("price");
                String From = room.optString("available_from");
                String To = room.optString("available_to");
                String numBeds = room.optString("num_of_beds");
                String Avail = room.optString("available_beds");
                //String gender = jsonObj.getString("gender");
                int inadv1= jsonObj.getInt("inadvance_pay");
                int balcon= room.optInt("has_balcony");
                int bath= room.optInt("has_bathroom");
                int Maintain1 = jsonObj.getInt("including_maintenance");

                //String rate = jsonObj.getString("rate");

                textViewJSON1.setText(numBeds);
                textViewJSON2.setText(Avail);
                textViewJSON3.setText(used_as);
                textViewJSON6.setText(From);
                textViewJSON7.setText(To);
                textViewJSON8.setText(Title1);
                textViewJSON10.setText(body1);
                textViewJSON11.setText(price1);


                if(balcon==1){
                    textViewJSON5.check(R.id.B_No);

                }
                else  if(balcon==0){
                    textViewJSON5.check(R.id.B_yes);
                }



                if(bath==1){
                    textViewJSON4.check(R.id.BT_No);

                }
                else  if(bath==0){
                    textViewJSON4.check(R.id.BT_yes);
                }



                if(inadv1==1){
                    textViewJSON12.check(R.id.Recently);

                }
                else  if(inadv1==0){
                    textViewJSON12.check(R.id.Indavance);
                }




                if(Maintain1==1){
                    textViewJSON13.check(R.id.M_No);
                }
                else  if(Maintain1==0){
                    textViewJSON13.check(R.id.M_yes);
                }


                /*textViewJSON9.setText(educational_level);
                textViewJSON10.setText(age);
                textViewJSON11.setText(gender);
                textViewJSON12.setText(rate);*/
                //textViewJSON13.setText(marital_status);


            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error parsing JSON data.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Couldn't get any JSON data.", Toast.LENGTH_SHORT).show();
        }
    }

}
