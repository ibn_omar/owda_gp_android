package com.example.ibnomar.owda;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import android.net.ConnectivityManager;
import org.json.JSONException;
import android.app.ProgressDialog;
import org.json.JSONObject;
import android.os.Bundle;
import java.util.regex.Matcher;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.regex.Pattern;
import android.content.Context;
import android.widget.AdapterView;
import android.widget.TextView;
import java.util.ArrayList;
import org.json.JSONArray;
import android.widget.AdapterView.OnItemSelectedListener;


public class sign_up1 extends AppCompatActivity {
    ArrayList<String> worldlist;
    //String[] country = {"India", "USA", "China", "Japan", "Other"};
    //String [] country  = new String [242]  ;
    Button btnReg;
    EditText edtUser, edtPass, edtConfirmPass, edtEmail, edtFirst, edtLast, edtAge, edtPhone, edtFBAccount;
    TextView test;
    RadioGroup radioSex, mStatus, smoke, education;
    RadioButton rSex, rStatus, rSmoke, rEducation;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up1);

        //initialization of all editText
        edtUser = (EditText) findViewById(R.id.edtUsername);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPass = (EditText) findViewById(R.id.edtPass);
        edtConfirmPass = (EditText) findViewById(R.id.edtConfirmPass);
        //Initialization of Register Button
        btnReg = (Button) findViewById(R.id.button1);
        test = (TextView) findViewById(R.id.test);
        edtFirst = (EditText) findViewById(R.id.edtFirst);
        edtLast = (EditText) findViewById(R.id.edtLast);
        edtAge = (EditText) findViewById(R.id.edtage);
        edtPhone = (EditText) findViewById(R.id.edtPhone);
        edtFBAccount = (EditText) findViewById(R.id.edtFBAccount);
        //edtEducation=(EditText)findViewById(R.id.edtEduction);
        radioSex = (RadioGroup) findViewById(R.id.radioSex);
        mStatus = (RadioGroup) findViewById(R.id.mStatus);
        smoke = (RadioGroup) findViewById(R.id.smoke);
        education = (RadioGroup) findViewById(R.id.education);
        // Spinner element
        final Spinner spin = (Spinner) findViewById(R.id.spinner1);
        // spin.setOnItemSelectedListener(this);
        /*ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, worldlist);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);
        //Registration button functionality*/
        String Country_id []=new String[242];
        String Country_name []=new String[242];
        Bundle extras = getIntent().getExtras();
        Country_id = extras.getStringArray("id");
        Country_name = extras.getStringArray("name");
        Spinner mySpinner = (Spinner) findViewById(R.id.spinner1);
        mySpinner.setAdapter(new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_dropdown_item,
                        Country_name));
        btnReg.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int radioButtonID1 = radioSex.getCheckedRadioButtonId();
                View radioButton1 = radioSex.findViewById(radioButtonID1);
                int rS = radioSex.indexOfChild(radioButton1);
                //int gender =radioSex.getCheckedRadioButtonId();

                int radioButtonID2 = mStatus.getCheckedRadioButtonId();
                View radioButton2 = mStatus.findViewById(radioButtonID2);
                int rST = mStatus.indexOfChild(radioButton2);
                //int status =mStatus.getCheckedRadioButtonId();

                int radioButtonID3 = smoke.getCheckedRadioButtonId();
                View radioButton3 = smoke.findViewById(radioButtonID3);
                int rSM = smoke.indexOfChild(radioButton3);
                //int Smoke =smoke.getCheckedRadioButtonId();

                int radioButtonID4 = education.getCheckedRadioButtonId();
                View radioButton4 = education.findViewById(radioButtonID4);
                int rE = education.indexOfChild(radioButton4) + 1;
                //int edu =education.getCheckedRadioButtonId();

                String u = edtUser.getText().toString();
                String E = edtEmail.getText().toString();
                String P = edtPass.getText().toString();
                String F = edtFirst.getText().toString();
                String L = edtLast.getText().toString();
                String A = edtAge.getText().toString();
                String PH = edtPhone.getText().toString();
                String rt = "4";
                String Sp = spin.getSelectedItem().toString();
                int country;
                //int country = 1;
                if (Sp=="Montenegro"){
                    country=243;
                }
                if (Sp=="Serbia"){
                    country=386;
                }
                else{
                    country=spin.getSelectedItemPosition()+1;
                }
                String FA = edtFBAccount.getText().toString();
                String pic = "null";

                SignUp1(v, F, L, pic, u, P, PH, E, country, FA, rE, A, rS, rt, rST, rSM);
            }
        });
    }


    public void SignUp1(View v, String First, String Last, String Picture, String UserName, String Password, String Phone, String Email, int Country, String Account, int Education, String Age, int Sex, String rate, int Status, int Smoke) {

        Boolean t = isNetworkConnected();
        Boolean E = isEmailValid(Email);
        if (!Email.equals("") && !UserName.equals("") && !Password.equals("") && !Age.equals("") && !First.equals("") && !Last.equals("") && !Phone.equals("") && Country != -1) {
            if (!edtPass.getText().toString().equals(edtConfirmPass.getText().toString())) {
                edtConfirmPass.setError("Password Not matched");
                edtConfirmPass.requestFocus();
            } else if (edtPass.getText().toString().length() < 8) {
                edtPass.setError("Password should be atleast of 8 charactors");
                edtPass.requestFocus();
            } else if (E != true) {
                edtEmail.setError("Email should be Contain @ or .  ");
                edtEmail.requestFocus();
            } else {
                if (t == true) {
                    new SignUpClass(this, test).execute(First, Last, Picture, UserName, Password, Phone, Email, String.valueOf(Country), Account, String.valueOf(Education), Age, String.valueOf(Sex), rate, String.valueOf(Status), String.valueOf(Smoke));


                } else {
                    //test.setText("Network Failed");
                    Context context = getApplicationContext();
                    CharSequence text = "Connection error";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                }
            }
        } else {

            /*Context context = getApplicationContext();
            CharSequence text = "Please Fill all Fields";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();*/
            if (edtUser.getText().toString().length() == 0) {
                edtUser.setError("Username is Required");
                edtUser.requestFocus();
            } else if (edtPass.getText().toString().length() == 0) {
                edtPass.setError("Password not entered");
                edtPass.requestFocus();
            } else if (edtConfirmPass.getText().toString().length() == 0) {
                edtConfirmPass.setError("Please confirm password");
                edtConfirmPass.requestFocus();
            } else if (!edtPass.getText().toString().equals(edtConfirmPass.getText().toString())) {
                edtConfirmPass.setError("Password Not matched");
                edtConfirmPass.requestFocus();
            } else if (edtPass.getText().toString().length() < 8) {
                edtPass.setError("Password should be atleast of 8 charactors");
                edtPass.requestFocus();


            } else if (edtEmail.getText().toString().length() == 0) {
                edtEmail.setError("Email is Required");
                edtEmail.requestFocus();
            } else if (edtFirst.getText().toString().length() == 0) {
                edtFirst.setError("First name not entered");
                edtFirst.requestFocus();
            } else if (edtLast.getText().toString().length() == 0) {
                edtLast.setError("Last name not entered");
                edtLast.requestFocus();
            } else if (edtAge.getText().toString().length() == 0) {
                edtAge.setError("Age is Required");
                edtAge.requestFocus();
            } else if (edtPhone.getText().toString().length() == 0) {
                edtPhone.setError("Phone is Required");
                edtPhone.requestFocus();
            } /*else if (edtFBAccount.getText().toString().length() == 0) {
                edtFBAccount.setError("Facebook Account is Required");
                edtFBAccount.requestFocus();
            }*/

        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    @Override
    public void onBackPressed() {
        /*Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);*/
        Intent i = new Intent(getApplicationContext(),splashScreen.class);
        startActivity(i);
        //finish();
    }

    }

