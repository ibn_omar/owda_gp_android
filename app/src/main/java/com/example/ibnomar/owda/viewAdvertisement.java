package com.example.ibnomar.owda;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;

public class viewAdvertisement extends AppCompatActivity {

    TextView Title1;
    TextView body1;
    TextView price1;
    TextView inadv1;
    TextView Maintain1;
    TextView ExDate1;
    TextView text;
    TextView text2;
    Context context;
    String Adv_id ;
    String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

       // Toast.makeText(context, "Data inserted successfully. Signup completed successfully.", Toast.LENGTH_SHORT).show();
        Boolean t = isNetworkConnected();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_advertisement);
        Title1 = (TextView) findViewById(R.id.Title1);
        body1 = (TextView) findViewById(R.id.body1);
        price1 = (TextView) findViewById(R.id.price1);
        //beds1 = (TextView) findViewById(R.id.beds1);
        //text5 = (TextView) findViewById(R.id.text5);
        inadv1 = (TextView) findViewById(R.id.inadv1);
        Maintain1 = (TextView) findViewById(R.id.Maintain1);
        ExDate1 = (TextView) findViewById(R.id.ExDate1);
        Bundle extras = getIntent().getExtras();
         Adv_id = extras.getString("Adv_id");
        user_id = extras.getString("user_id");
        //final String adv_id = extras.getString("adv_id");
        /*text2.setText(adv_id);
        Toast.makeText(context, adv_id, Toast.LENGTH_SHORT).show();*/


        new ViewAdClass(this,Title1,body1,price1,inadv1,Maintain1,ExDate1).execute(Adv_id);

        Button button = (Button) findViewById(R.id.EditAdv);
        button.setOnClickListener(new View.OnClickListener() {

            Boolean t = isNetworkConnected();

            @Override
            public void onClick(View v) {
                if (t == true) {
                    Intent i = new Intent(getApplicationContext(), editAdvertisement.class);
                    /*Bundle extras = getIntent().getExtras();
                    String id = extras.getString("id");
                    i.putExtra("id", id);*/
                    i.putExtra("Adv_id", Adv_id);
                    i.putExtra("user_id", user_id);
                    startActivity(i);
                }
                else {
                    //text.setText("Connection error");
                    //Toast.makeText(context1, "Connection error", Toast.LENGTH_SHORT).show();
                    Context context = getApplicationContext();
                    CharSequence text = "Connection error";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                }


            }

        });
    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


}
